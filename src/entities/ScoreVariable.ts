import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { Score } from './Score';
import { Variable } from './Variable';

@Entity('score_variable')
export class ScoreVariable {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;


    @Column('float8', {
        nullable: false,
        name: 'score_value'
    })
    ScoreValue: number;


    @Column('float8', {
        nullable: true,
        name: 'variable_weigth'
    })
    VariableWeigth: number;


    @Column('timestamp without time zone', {
        nullable: false,
        name: 'created_at'
    })
    CreatedAt: Date;

    @ManyToOne(type => Score, Score => Score.ScoreVariables, {  nullable: false, })
    @JoinColumn({ name: 'score_id'})
    Score: Score | null;

    @ManyToOne(type => Variable, Variable => Variable.ScoreVariables, {  nullable: false, })
    @JoinColumn({ name: 'variable_id'})
    Variable: Variable | null;

}
