import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { ScoreVariable } from './ScoreVariable';
import { VariableParameter } from './VariableParameter';
import { Variable } from './Variable';
import { Category } from './Category';

@Entity('category_variable')
export class CategoryVariable {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;


    @Column('boolean', {
        nullable: true,
        name: 'active'
    })
    Active: boolean;

    @ManyToOne(type => Variable, Variable => Variable.Categories, {  nullable: true, })
    @JoinColumn({ name: 'variable_id'})
    Variable: Variable | null;

    @ManyToOne(type => Category, Category => Category.Categories, {  nullable: true, })
    @JoinColumn({ name: 'category_id'})
    Category: Category | null;

}
