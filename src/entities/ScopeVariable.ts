import {  Entity,  PrimaryGeneratedColumn, Column,   ManyToOne,  JoinColumn} from 'typeorm';
import { Variable } from './Variable';
import {Scope} from "./Scope";
import {GlobalValidator} from "../validators/GlobalValidator";

@Entity('scope_variable')
export class ScopeVariable extends GlobalValidator{

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;

    @Column('timestamp without time zone', {
        nullable: false,
        name: 'created_at'
    })
    CreatedAt: Date;

    @ManyToOne(type => Scope, Scope => Scope.ScopeVariables, {  nullable: false, })
    @JoinColumn({ name: 'scope_id'})
    Scope: Scope | null;

    @ManyToOne(type => Variable, Variable => Variable.ScopeVariables, {  nullable: false, })
    @JoinColumn({ name: 'variable_id'})
    Variable: Variable | null;

}
