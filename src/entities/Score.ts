import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { ScoreVariable } from './ScoreVariable';
import { ScopeScore } from './ScopeScore';
import { Summary } from './Summary';


@Entity('score')
export class Score {

    @PrimaryGeneratedColumn({
        name: 'id'
        })
    Id: number;


    @Column('int4', {
        nullable: false,
        name: 'owner_contract_id'
        })
    OwnerContractId: number;


    @Column('float8', {
        nullable: true,
        name: 'score_value'
    })
    ScoreValue: number;


    @Column('character varying', {
        nullable: false,
        name: 'description'
    })
    Description: string;


    @Column('int4', {
        nullable: true,
        name: 'comparison_with_last_time'
        })
    ComparisonWithLastTime: number;


    @Column('timestamp without time zone', {
        nullable: true,
        name: 'date'
    })
    Date: Date;


    @Column('timestamp without time zone', {
        nullable: false,
        name: 'created_at'
    })
    CreatedAt: Date;
    // virtual reference to group for distinct each score from indicators.
    @Column('int4', {
        nullable: true,
        name: 'group_id'
        })
    groupId: number;

    @OneToMany(type => ScoreVariable, ScoreVariable => ScoreVariable.Score)
    ScoreVariables: ScoreVariable[];

    @OneToMany(type => ScopeScore, ScopeScore => ScopeScore.Score,{ cascade: true})
    ScopeScores: ScopeScore[];

}
