import {  Entity,  PrimaryGeneratedColumn, Column,   ManyToOne,  JoinColumn, Index} from 'typeorm';
import {Score} from "./Score";
import {GlobalValidator} from "../validators/GlobalValidator";
import {Scope} from "./Scope";
import {Group} from "./Group";

@Entity('scope_score')
export class ScopeScore extends GlobalValidator{

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;


    @Column('float8', {
        nullable: false,
        name: 'score_value'
    })
    ScoreValue: number;

    @Index()
    @Column('timestamp without time zone', {
        nullable: false,
        name: 'created_at',
        default: () => "CURRENT_TIMESTAMP"
    })
    CreatedAt: Date;

    @Column('jsonb', {
        nullable: false,
        name: 'operation_data'
    })
    OperationData: Object;

    @Index()
    @ManyToOne(type => Scope, Scope => Scope.ScopeScores, {  nullable: false, })
    @JoinColumn({ name: 'scope_id'})
    Scope: Scope | null;

    @Index()
    @ManyToOne(type => Score, Score => Score.ScopeScores, {  nullable: false, })
    @JoinColumn({ name: 'score_id'})
    Score: Score| null;

}
