import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { ScoreVariable } from './ScoreVariable';
import { VariableParameter } from './VariableParameter';
import { Group } from './Group';
import { CategoryVariable } from './CategoryVariable';
import { ScopeVariable } from './ScopeVariable';

@Entity('variable')
export class Variable {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;

    @Column('character varying', {
        nullable: false,
        name: 'label'
    })
    Label: string;

    @Column('character varying', {
        nullable: false,
        name: 'name'
    })
    Name: string;

    @Column('character varying', {
        nullable: false,
        name: 'type_micro_service'
    })
    TypeMicroService: string;

    @Column('character varying', {
        nullable: false,
        name: 'end_point'
    })
    EndPoint: string;

    @Column('character varying', {
        nullable: true,
        name: 'http_method'
    })
    HttpMethod: string;

    @Column('boolean', {
        nullable: true,
        name: 'is_array',
        default: false
    })
    IsArray: boolean;


    @Column('int4', {
        nullable: false,
        name: 'value'
    })
    Value: number;


    @Column('boolean', {
        nullable: false,
        name: 'active'
    })
    Active: boolean;


    @Column('int4', {
        nullable: true,
        name: 'enterprise_id'
    })
    EnterpriseId: number;


    @Column('timestamp without time zone', {
        nullable: false,
        name: 'created_at'
    })
    CreatedAt: Date;

    @ManyToOne(type => Group, Group => Group.Variables, {  nullable: true, })
    @JoinColumn({ name: 'group_id'})
    Group: Group | null;

    @OneToMany(type => ScoreVariable, ScoreVariable => ScoreVariable.Variable)
    ScoreVariables: ScoreVariable[];

    @OneToMany(type => VariableParameter, VariableParameter => VariableParameter.Variable)
    VariableParameters: VariableParameter[];

    @OneToMany(type => CategoryVariable, CategoryVariable => CategoryVariable.Variable)
    Categories: CategoryVariable[];
    
    @OneToMany(type => ScopeVariable, ScopeVariable => ScopeVariable.Variable)
    ScopeVariables: ScopeVariable[];

}
