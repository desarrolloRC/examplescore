import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { ScoreVariable } from './ScoreVariable';
import { Variable } from './Variable';

@Entity('variable_parameter')
export class VariableParameter {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;

    @Column('jsonb', {
        nullable: false,
        name: 'parameters'
    })
    Parameters: string;


    @Column('boolean', {
        nullable: false,
        name: 'active'
    })
    Active: boolean;


    @Column('timestamp without time zone', {
        nullable: false,
        name: 'created_at'
    })
    CreatedAt: Date;

    @ManyToOne(type => Variable, Variable => Variable.VariableParameters, {  nullable: true, })
    @JoinColumn({ name: 'variable_id'})
    Variable: Variable | null;

}
