import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { ScoreVariable } from './ScoreVariable';
import { VariableParameter } from './VariableParameter';
import { Variable } from './Variable';
import { CategoryVariable } from './CategoryVariable';

@Entity('category')
export class Category {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;

    @Column('character varying', {
        nullable: false,
        name: 'label'
    })
    Label: string;

    @Column('character varying', {
        nullable: false,
        name: 'name'
    })
    Name: string;

    @OneToMany(type => Variable, Variable => Variable.Group)
    Variables: Variable[];

    @OneToMany(type => CategoryVariable, CategoryVariable => CategoryVariable.Category)
    Categories: CategoryVariable[];

}
