import { Index, Entity, PrimaryColumn, PrimaryGeneratedColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from 'typeorm';
import { ScoreVariable } from './ScoreVariable';
import { VariableParameter } from './VariableParameter';
import { Variable } from './Variable';
import { Scope } from './Scope';

@Entity('group')
export class Group {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;

    @Column('character varying', {
        nullable: false,
        name: 'label'
    })
    Label: string;

    @Column('int4', {
        nullable: true,
        name: 'enterprise_id'
    })
    EnterpriseId: number;

    @Column('character varying', {
        nullable: false,
        name: 'name'
    })
    Name: string;

    @Column('int4', {
        nullable: true,
        name: 'value'
    })
    Value: number;

    @Column('boolean', {
        nullable: true,
        name: 'active'
    })
    Active: number;

    @OneToMany(type => Variable, Variable => Variable.Group)
    Variables: Variable[];
    
    @OneToMany(type => Scope, Scope => Scope.Group)
    Scopes: Scope[];
}
