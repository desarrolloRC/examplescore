import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn} from 'typeorm';
import {ScopeScore} from "./ScopeScore";
import {ScopeVariable} from "./ScopeVariable";
import {GlobalValidator} from "../validators/GlobalValidator";
import {Variable} from "./Variable";
import {Group} from "./Group";

@Entity('scope')
export class Scope extends GlobalValidator{

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;



    @Column('character varying', {
        nullable: false,
        name: 'label'
    })
    Label: string;

    @Column('character varying', {
        nullable: false,
        name: 'name'
    })
    Name: string;

    @Column('float8', {
        nullable: false,
        name: 'value'
    })
    Value: number;


    @Column('boolean', {
        nullable: false,
        name: 'active'
    })
    Active: boolean;

    @Column('character varying', {
        nullable: false,
        name: 'expression'
    })
    Expression: string;

    @OneToMany(type => ScopeScore, ScoreScope => ScoreScope.Score)
    ScopeScores: ScopeScore[];

    @OneToMany(type => ScopeVariable, ScopeVariable => ScopeVariable.Scope)
    ScopeVariables: ScopeVariable[];

    @ManyToOne(type => Group, Group => Group.Scopes, {  nullable: true, })
    @JoinColumn({ name: 'group_id'})
    Group: Group | null;

}
