import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn } from 'typeorm';
import { Score } from './Score';

@Entity('summary')
export class Summary {

    @PrimaryGeneratedColumn({
        name: 'id'
    })
    Id: number;

    @Column('int4', {
        nullable: false,
        name: 'owner_contract_id'
    })
    OwnerContract: number;

    @Column('int4', {
        nullable: true,
        name: 'vehicle_id'
    })
    VehicleId: number;

    @Column('character varying', {
        nullable: false,
        name: 'plate'
    })
    Plate: number;

    @Column('character varying', {
        nullable: true,
        name: 'driver'
    })
    Driver: string;

    @Column('character varying', {
        nullable: false,
        name: 'contract_number'
    })
    ContractNumber: string;

    @Column('int4', {
        nullable: true,
        name: 'harvest'
    })
    Harvest: number;

    @Column('int4', {
        nullable: false,
        name: 'enterprise_id'
    })
    Enterprise: number;

    @Column('date', {
        nullable: true,
        name: 'date'
    })
    Date: string;

    @Column('float8', {
        nullable: true,
        name: 'tickets_pending'
    })
    tickets_pending: number;

    @Column('float8', {
        nullable: true,
        name: 'amount_tickets_pending'
    })
    amount_tickets_pending: number;

    @Column('character varying', {
        nullable: true,
        name: 'last_date_gps'
    })
    last_date_gps: string;

    @Column('character varying', {
        nullable: true,
        name: 'last_location_gps'
    })
    last_location_gps: string;

    @Column('float8', {
        nullable: true,
        name: 'engine_hours'
    })
    engine_hours: number;

    @Column('float8', {
        nullable: true,
        name: 'average_speed'
    })
    average_speed: number;

    @Column('float8', {
        nullable: true,
        name: 'top_speed'
    })
    top_speed: number;

    @Column('float8', {
        nullable: true,
        name: 'kilometers'
    })
    kilometers: number;

    @Column('float8', {
        nullable: true,
        name: 'times_beyond_limit'
    })
    times_beyond_limit: number;

    @Column('character varying', {
        nullable: true,
        name: 'last_check_date'
    })
    last_check_date: string;

    @Column('float8', {
        nullable: true,
        name: 'last_check_score'
    })
    last_check_score: number;

    @Column('float8', {
        nullable: true,
        name: 'documents_on_risk'
    })
    documents_on_risk: number;

    @Column('float8', {
        nullable: true,
        name: 'documents_expired'
    })
    documents_expired: number;

    @Column('float8', {
        nullable: true,
        name: 'payment_freuency'
    })
    payment_freuency: number;

    @Column('float8', {
        nullable: true,
        name: 'debt_amount'
    })
    debt_amount: number;

    @Column('float8', {
        nullable: true,
        name: 'debt_days'
    })
    debt_days: number;

    @Column('float8', {
        nullable: true,
        name: 'payment_percentage'
    })
    payment_percentage: number;

    @Column('float8', {
        nullable: true,
        name: 'ideal_payment_percentage'
    })
    ideal_payment_percentage: number;

    @Column('float8', {
        nullable: true,
        name: 'kilometers_ideal'
    })
    kilometers_ideal: number;

    @Column('float8', {
        nullable: true,
        name: 'today_check'
    })
    today_check: number;


    @Column('integer', {
        nullable: true,
        name: 'license_score'
    })
    LicenseScore: number;


    @Column('decimal', {
        nullable: true,
        name: 'payment_score'
    })
    PaymentScore: number;


    @Column('float8', {
        nullable: true,
        name: 'ranking'
    })
    Ranking: number;


    @Column('text', {
        nullable: true,
        name: 'bucket'
    })
    Bucket: string;


    @Column('float8', {
        nullable: true,
        name: 'past_due_balance'
    })
    PastDueBalance: string;


    @Column('float8', {
        nullable: true,
        name: 'reserve'
    })
    Reserve: string;


    @Column('float8', {
        nullable: true,
        name: 'accrued_interest'
    })
    AccruedInterest: string;


    @Column('float8', {
        nullable: true,
        name: 'ranking_score'
    })
    RankingScore: number;


    @Column('float8', {
        nullable: true,
        name: 'initial_debt'
    })
    InitialDebt: string;


    @Column('float8', {
        nullable: true,
        name: 'last_score_id'
    })
    last_score_id: number;


}
