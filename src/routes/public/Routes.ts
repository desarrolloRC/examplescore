import * as Router from 'koa-router';

// Local controllers
import controller = require('../../controllers/Index');

const routerPublic = new Router();

const initialPath = '/api/public/';

// Api health check
routerPublic.get(`${initialPath}health`, controller.Public.healthCheck);
routerPublic.get(`${initialPath}calculate-score-test`, controller.Score.calculateScoreTest);
routerPublic.get(`${initialPath}calculate-score-test-v2`, controller.Score.calculateScoreTestV2);
routerPublic.get(`${initialPath}calculate-summary-test`, controller.Score.calculateSummaryTest);


export { routerPublic };
