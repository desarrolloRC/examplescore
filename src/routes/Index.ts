// Router libraries
import * as CombineRouter from 'koa-combine-routers';

// Local routes
import { routerScore } from './api/Routes';
import { routerPublic } from './public/Routes';

const router = CombineRouter(
    routerScore,
    routerPublic,
);

export { router };
