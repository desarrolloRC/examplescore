import * as Router from 'koa-router';

// Local controllers
import controller = require('../../controllers/Index');

const routerScore = new Router();

const initialPath = '/api/v1/';

// Api get all check types

routerScore.post(`${initialPath}get-vehicles-in-risk`, controller.Score.getVehiclesInRisk);

routerScore.post(`${initialPath}get-vehicles-in-risk-all`, controller.Score.getVehiclesInRiskAll);

routerScore.post(`${initialPath}get-score-by-owner`, controller.Score.getScoreByOwner);

routerScore.get(`${initialPath}get-vehicles-in-risk-table`, controller.Score.getVehiclesInRiskTable);

routerScore.get(`${initialPath}get-drivers-ranking`, controller.Score.getDriversRanking);

routerScore.post(`${initialPath}get-drivers-ranking-all`, controller.Score.getDriversRankingAll);

routerScore.get(`${initialPath}get-vehicles-in-risk-table-all`, controller.Score.getVehiclesInRiskTableAll);

routerScore.post(`${initialPath}get-distance-widget`, controller.Score.getDistanceWidget);

routerScore.post(`${initialPath}get-distance-widget-report`, controller.Score.getDistanceWidgetReport);

routerScore.post(`${initialPath}get-distance-widget-low-report`, controller.Score.getDistanceWidgetLowReport);

routerScore.post(`${initialPath}get-distance-widget-none-report`, controller.Score.getDistanceWidgetNoneReport);

routerScore.post(`${initialPath}get-top-speed-widget`, controller.Score.getTopSpeedWidget);

routerScore.post(`${initialPath}get-gps-widget`, controller.Score.getGpsWidget);

routerScore.post(`${initialPath}get-last-location`, controller.Score.getLastLocation);

routerScore.post(`${initialPath}get-last-location-report`, controller.Score.getLastLocationReport);

routerScore.post(`${initialPath}calculate-value-variable`, controller.Score.calculateValueForVariable);

routerScore.post(`${initialPath}get-speed-limit-by-params`, controller.Score.getSpeedLimitByParams);

routerScore.post(`${initialPath}get-summary-by-plate`, controller.Score.getSummaryByPlate);

routerScore.post(`${initialPath}get-percentage-difference`, controller.Score.getPercentageDifference);

routerScore.post(`${initialPath}calculate-summary`, controller.Score.calculateSummary);

routerScore.post(`${initialPath}get-Kilometers-report`, controller.Score.getKilometersReport);

routerScore.post(`${initialPath}get-chart-license-points`, controller.Score.getChartLicensePoints);

routerScore.post(`${initialPath}get-chart-gps-operation`, controller.Score.getGpsOperation);

routerScore.post(`${initialPath}get-scatter-chart`, controller.Score.getScatterChart);

routerScore.post(`${initialPath}update-payment-score`, controller.Score.updatePaymentScore);

routerScore.post(`${initialPath}segmentation-chart`, controller.Summary.segmentationChart);

routerScore.post(`${initialPath}segmentation-chart-line`, controller.Summary.segmentationChartLine);

routerScore.post(`${initialPath}segmentation-chart-line-avg`, controller.Summary.segmentationAvgChartLine);

routerScore.post(`${initialPath}bulk/balance`, controller.Summary.bulkContractBalance);

routerScore.post(`${initialPath}bulk/last-debt`, controller.Summary.bulkLastDebt);

export { routerScore };
