import * as dotenv from 'dotenv';

import { IConfig } from './interfaces/IconfigInterface';

dotenv.config();

const db = process.env.DEPLOY === 'AWS' ?
    `posgres://${process.env.POSTGRES_USER}:${process.env.POSTGRES_PASSWORD}@${process.env.POSTGRES_HOST}:${process.env.POSTGRES_PORT}/${process.env.POSTGRES_DB}` :
    `posgres://${process.env.RDS_USERNAME}:${process.env.RDS_PASSWORD}@${process.env.RDS_HOSTNAME}:${process.env.RDS_PORT}/${process.env.RDS_DB_NAME}`;

const config: IConfig = {
    port: parseInt(process.env.PORT),
    debugLogging: process.env.NODE_ENV === 'development',
    dbsslconn: process.env.NODE_ENV !== 'development',
    databaseUrl: db,
};

export { config };
