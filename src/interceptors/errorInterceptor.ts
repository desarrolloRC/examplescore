import { ResponseHelper } from '../helpers/ResponseHelper';

export async function errorInterceptor(ctx, next) {
    try {
        await next();
    } catch (err) {
        console.warn(err);

        return ResponseHelper.createResponse(ctx, err.message, 500);
    }
}

