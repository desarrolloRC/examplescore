import { BaseContext } from 'koa';
import * as HttpStatus from 'http-status-codes';

export default class SiteController {
    public static async healthCheck(ctx: BaseContext) {
        ctx.body = {status: 'OK'};
        ctx.response.status = HttpStatus.OK;
    }
}
