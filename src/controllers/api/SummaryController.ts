import { BaseContext } from 'koa';

import { JwtHelper } from '../../helpers/JwtHelper';
import { VehicleHelper } from '../../helpers/VehicleHelper';
import { SummaryManager } from '../../managers/SummaryManager';
import { CollectionHelper } from '../../helpers/CollectionHelper';
import { SegmentationHelper } from '../../helpers/SegmentationHelper';
import { ContractBalanceInterface } from '../../interfaces/ContractBalanceInterface';
import { ContractLastDebtInterface } from '../../interfaces/ContractLastDebtInterface';

export default class SummaryController {

    /**
     *
     * @param ctx
     */
    public static async segmentationChartLine(ctx: BaseContext) {
        const token = ctx.request.header.authorization;
        const decodeToken = await JwtHelper.decode(token);
        const data: {
            harvest: any;
            search: string;
            endDate: string;
            startDate: string;
            enterprise: number;
            contracts: number[]
        } = ctx.request.body;

        data.enterprise = decodeToken['enterprise'];

        const contracts = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(data.enterprise, data, token);
        const segmentationDataReport = await SummaryManager.getSegmentationDataReport(data, contracts);
        const segmentationData = await SummaryManager.getSegmentationData(data, contracts);
        const segmentationDataFinal = [];

        segmentationDataFinal.push(['Dia', '0-30', '30-60', '60-90', '90 +']);
        let endDate = new Date(data.endDate);
        for (var d = new Date(data.startDate); d <= endDate; d.setDate(d.getDate() + 1)) {
            let dateFind = new Date(d);
            let array = [
                dateFind,
                0, // 0-30
                0, // 30-60
                0, // 60-90
                0 // 90+
            ];

            segmentationData.map((value: any) => {
                const dateValue = new Date(value.date);
                let dateFirst = `${dateFind.getFullYear()}-${(dateFind.getMonth() + 1)}-${dateFind.getDate()}`;
                let dateSecond = `${dateValue.getFullYear()}-${(dateValue.getMonth() + 1)}-${dateValue.getDate()}`;
                if (dateSecond === dateFirst) {
                    switch (value.estado) {
                        case '0-30':
                            array[1] = parseInt(value.cantidad, 0);
                        break;
                        case '30-60':
                            array[2] = parseInt(value.cantidad, 0);
                        break;
                        case '60-90':
                            array[3] = parseInt(value.cantidad, 0);
                        break;
                        case '90 +':
                            array[4] = parseInt(value.cantidad, 0);
                        break;
                    }
                }
            });
            segmentationDataFinal.push(array);

        }
        

        return ctx.body = {data: segmentationDataFinal, dataAll: segmentationDataReport, value: 0};

    }

    public static async segmentationAvgChartLine(ctx: BaseContext) {
        const token = ctx.request.header.authorization;
        const decodeToken = await JwtHelper.decode(token);
        const data: {
            harvest: any;
            search: string;
            endDate: string;
            startDate: string;
            enterprise: number;
            contracts: number[]
        } = ctx.request.body;

        data.enterprise = decodeToken['enterprise'];

        const contracts = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(data.enterprise, data, token);
        const segmentationDataReport = await SummaryManager.getSegmentationDataReport(data, contracts);
        const segmentationData = await SummaryManager.getSegmentationDataAvg(data, contracts);
        const segmentationDataFinal = [];

        segmentationDataFinal.push(['Dia', '0-30', '30-60', '60-90', '90 +']);
        let endDate = new Date(data.endDate);
        for (var d = new Date(data.startDate); d <= endDate; d.setDate(d.getDate() + 1)) {
            let dateFind = new Date(d);
            let array = [
                dateFind,
                0, // 0-30
                0, // 30-60
                0, // 60-90
                0 // 90+
            ];

            segmentationData.map((value: any) => {
                const dateValue = new Date(value.date);
                let dateFirst = `${dateFind.getFullYear()}-${(dateFind.getMonth() + 1)}-${dateFind.getDate()}`;
                let dateSecond = `${dateValue.getFullYear()}-${(dateValue.getMonth() + 1)}-${dateValue.getDate()}`;
                if (dateSecond === dateFirst) {
                    switch (value.estado) {
                        case '0-30':
                            array[1] = parseInt(value.monto, 0);
                        break;
                        case '30-60':
                            array[2] = parseInt(value.monto, 0);
                        break;
                        case '60-90':
                            array[3] = parseInt(value.monto, 0);
                        break;
                        case '90 +':
                            array[4] = parseInt(value.monto, 0);
                        break;
                    }
                }
            });
            segmentationDataFinal.push(array);

        }
        

        return ctx.body = {data: segmentationDataFinal, dataAll: segmentationDataReport, value: 0};

    }
    /**
     *
     * @param ctx
     */
    public static async segmentationChart(ctx: BaseContext) {
        const token = ctx.request.header.authorization;
        const decodeToken = await JwtHelper.decode(token);
        const data: {
            harvest: any;
            search: string;
            endDate: string;
            startDate: string;
            enterprise: number;
            contracts: number[]
        } = ctx.request.body;

        data.enterprise = decodeToken['enterprise'];

        const contracts = await VehicleHelper.getContractBalance(data, token);
        const cont = contracts.data;
        if (cont && cont.length > 0) {
            const remapContracts = SegmentationHelper.mapContracts(cont);

            data.contracts = remapContracts;

            const paymentData = await CollectionHelper.getDebAndLastDebt(data, token);

            if (paymentData.data && paymentData.data.length > 0) {
                const response = SegmentationHelper.segmentedData(await SummaryManager.getSegmentation(data, remapContracts), cont, paymentData.data);
                const responseResult = [];
                response.allData.map((value: any) => {
                    const valueFormat = {
                        Conductor: value.conductor,
                        Contrato: value.contrato,
                        'Deuda anterior': value['deuda anterior'],
                        'Deuda fin de mes': value['deuda fin de mes'],
                        'Dias de deuda': value['días de deuda'],
                        Categoria: value.estado,
                        Estimacion: value.estimado,
                        'Id Contrato': value['id contrato'],
                        'Valor de la deuda': value['valor de la deuda'],
                    };
                    responseResult.push(valueFormat);
                });
                return ctx.body = {data: response.data, dataAll: responseResult, value: 0};
            } else {
                return ctx.body = {data: [], dataAll: [], value: 0};
            }
        } else {
            return ctx.body = {data: [], dataAll: [], value: 0};
        }
    }

    /**
     *
     * @param ctx
     */
    public static async bulkContractBalance(ctx: BaseContext) {
        const data: ContractBalanceInterface[] = ctx.request.body.balance;

        for (const item of data) {
            await SummaryManager.updateContractBalance(item);
        }

        ctx.body = 'ok';
    }

    /**
     *
     * @param ctx
     */
    public static async bulkLastDebt(ctx: BaseContext) {
        const data: ContractLastDebtInterface[] = ctx.request.body.lastDebt;

        for (const item of data) {
            await SummaryManager.updateContractLastDebt(item, 10);
        }

        ctx.body = 'ok';
    }
}
