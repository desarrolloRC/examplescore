import { BaseContext } from 'koa';
import * as HttpStatus from 'http-status-codes';
import { IndexablePage } from '@panderalabs/koa-pageable';

import { JwtHelper } from '../../helpers/JwtHelper';
import { SearchHelper } from '../../helpers/SearchHelper';
import { VehicleHelper } from '../../helpers/VehicleHelper';
import { ResponseHelper } from '../../helpers/ResponseHelper';
import { PaginationHelper } from '../../helpers/PaginationHelper';
import { SearchInterface } from '../../interfaces/SearchInterface';
import { PageableInterface } from '../../interfaces/PageableInterface';
import { getRepository, getConnection } from 'typeorm';
import { ScoreManager } from '../../managers/ScoreManager';
import { ScoreHelper } from '../../helpers/ScoreHelper';
import { Score } from '../Index';
import { Summary } from '../../entities/Summary';
import { CalculateScoreHelper } from '../../helpers/CalculateScoreHelper';
import * as PromiseBluebird from 'bluebird';


export default class ScoreController {


    public static async updatePaymentScore(ctx: BaseContext) {
        const params = ctx.request.body;
        const date = new Date(params.date);

        const previousDate = new Date(params.date);
        previousDate.setDate(date.getDate() - 30);

        const futureDate = new Date(params.date);
        futureDate.setDate(date.getDate() + 30);

        const previous = await ScoreManager.getPaymentScoreSummary(params.ownerContract, previousDate, date);

        const future = await ScoreManager.getPaymentScoreSummary(params.ownerContract, date, futureDate);
        previous.map(async (prev) => {
            await getConnection()
            .createQueryBuilder()
            .update(Summary)
            .set({
                PaymentScore: params.score
            })
            .where('id = :id', { id: prev['id'] })
            .execute();
        });

        future.map(async (fut) => {
            if (fut.payment_score === null) {
                await getConnection()
                .createQueryBuilder()
                .update(Summary)
                .set({
                    PaymentScore: params.score
                })
                .where('id = :id', { id: fut['id'] })
                .execute();
            }
        });

        ctx.body = {status: 'Ok'};
    }

    public static async getScatterChart(ctx: BaseContext) {

        const params = ctx.request.body;
        const tokenBearer = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(tokenBearer);

        const data = {
            globalSearch: params['search'],
            startDate: params['startDate'],
            endDate: params['endDate'],
            harvest: params['harvest']
        };
        const contracts = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, tokenBearer);
        let result = [];
        if (contracts !== '') {
            result =  await ScoreManager.getDataScatterChart(params['xVar'], params['yVar'], contracts, data.startDate);
        }

        const resultFormated = [];
        const resultFormatedContract = [];
        resultFormated.push([params['xVarLabel'], params['yVarLabel']]);
        resultFormatedContract.push([params['xVarLabel'], params['yVarLabel']]);

        if (params['filterY'] === true) {
            const valueVariable = await ScoreManager.getValueVariableByContracts(params['filterYName'], contracts, data.startDate);
            if (valueVariable.length > 0) {
                let maxValue = 0;
                ctx.body =  await PromiseBluebird.mapSeries(valueVariable, async function (value) {
                    if (value.score > maxValue) {
                        maxValue = value.score;
                    }
                    await PromiseBluebird.mapSeries(result, async function (valueFirst, indxFirst) {
                        if (valueFirst['owner_contract_id'] === value.owner_contract_id) {
                            result[indxFirst]['value'] = value.score;
                        }
                    }, {concurrency: 1});
                }, {concurrency: 1})
                .then(async function () {
                    result.map((value) => {
                        resultFormated.push(
                            [
                                parseFloat(((parseFloat(value['value']) * 1) / maxValue).toFixed(2)) ,
                                parseFloat(value[params['yVar']])
                            ]
                            );
                            resultFormatedContract.push(value['owner_contract_id']);
                    });
                    return {data: resultFormated, value: 0, resultFormatedContract};
                });
            } else {
                ctx.body = {data: resultFormated, value: 0, resultFormatedContract};
            }
        } else {
            result.map((value) => {

                resultFormated.push(
                    [
                        parseFloat(value[params['xVar']]),
                        parseFloat(value[params['yVar']])
                    ]);
                resultFormatedContract.push(value['owner_contract_id']);
            });

            ctx.body = {data: resultFormated, value: 0, resultFormatedContract};
        }
    }

    public static async getGpsOperation(ctx: BaseContext) {
        const params = ctx.request.body;
        const tokenBearer = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(tokenBearer);

        const data = {
            globalSearch: params['search'],
            endDate: params['endDate'],
            startDate: params['startDate'],
            harvest: params['harvest']
        };
        const contracts = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, tokenBearer);
        const gpsOperation =  await ScoreManager.getGpsOperation(contracts, data.startDate, data.endDate);

        const resultAll = await ScoreManager.getGpsOperationAll(contracts, data.startDate , data.endDate);
        const resultAllFormated = await VehicleHelper.fixDataIdsByContract(resultAll, tokenBearer);

        let total = 0;
        const resultFormated = [];
        resultFormated.push(['Categoria', 'Conteo']);
        gpsOperation.map((value) => {
            total += value.count;

            resultFormated.push([value.category, parseFloat(value.count)]);
        });

        ctx.body = {data: resultFormated, dataAll: resultAllFormated, value: 0};
    }
    public static async getChartLicensePoints(ctx: BaseContext) {
        const params = ctx.request.body;
        const tokenBearer = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(tokenBearer);

        const data = {
            globalSearch: params['search'],
            endDate: params['endDate'],
            startDate: params['startDate'],
            harvest: params['harvest']
        };
        const contracts = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, tokenBearer);
        const licensePoints =  await ScoreManager.getChartLicensePointsCategories(contracts, data.startDate, data.endDate);

        const resultAll = await ScoreManager.getChartLicensePointsCategoriesAll(contracts, data.startDate , data.endDate);
        const resultAllFormated = await VehicleHelper.fixDataIdsByContract(resultAll, tokenBearer);


        let total = 0;
        let totalUnder = 0;
        const resultFormated = [];
        resultFormated.push(['Categoria', 'Conteo']);
        licensePoints.map((value) => {
            total += value.count;
            if (value.category === '< 40') {
                totalUnder += value.count;
            }
            resultFormated.push([value.category, parseFloat(value.count)]);
        });

        ctx.body = {data: resultFormated, dataAll: resultAllFormated, value: Math.round((totalUnder * 100) / total)};
    }

    public static async calculateSummary(ctx: BaseContext) {
        const params = ctx.request.body;
        switch (params['variable']['name']) {
            case 'ranking':
                ctx.body = await CalculateScoreHelper.calculateRankingPosition(params);
                break;
            case 'ranking_score':
                ctx.body = await CalculateScoreHelper.calculateRankingScore(params);
                break;
            case 'last_score_id':
                ctx.body = await CalculateScoreHelper.calculateRankingLastScore(params);
                break;
        }
    }

    public static async getSpeedLimitByParams(ctx: BaseContext) {
        const params = ctx.request.body;
        const start = new Date();
        const end = new Date();
        const days = params['var_params']['days_query'];
        end.setDate(end.getDate() - days);
        const times = await ScoreManager.getTimesBeyondLimitByParams(params['id'], end, start);

        if (times[0]['times'] >= params['var_params']['greaterThan']) {
            ctx.body = 1;
        } else {
            ctx.body = 0;
        }
    }

    public static async calculateValueForVariable(ctx: BaseContext) {
        const params = ctx.request.body;
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);
        const dateType = params['date'] === 'month' ? ` date_part('month', s.date) ` : ` s.date `;
        const data: SearchInterface = {
            globalSearch: params['search'],
            endDate: params['endDate'],
            startDate: params['startDate'],
            harvest: params['harvest']
        };
        let vehicles;
        if (typeof params['ownerContract'] !== 'undefined') {
            vehicles = `${params['ownerContract']}`;
        } else {
            vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);
        }
        const dataParams = {
            date: dateType,
            column: '',
            where: ''
        };
        switch (params['variable']) {
            case 'paymentFrecuency':
              dataParams.column = ` round(CAST(avg(s.payment_freuency) as numeric), 2) `;
              dataParams.where = ` `;
              ctx.body = await ScoreManager.getDateGroupedInRange(data, vehicles, dataParams);
              break;
            case 'paymentPercentage':
              dataParams.column = ` round(CAST(avg(s.payment_percentage) as numeric), 2) `;
              dataParams.where = ` `;
              ctx.body = await ScoreManager.getDateGroupedInRange(data, vehicles, dataParams);
              break;
            case 'debtAmount':
              dataParams.column = ` round(CAST(avg(s.debt_amount) as numeric), 2) `;
              dataParams.where = ` and s.debt_amount > 0 `;
              ctx.body = await ScoreManager.getDateGroupedInRange(data, vehicles, dataParams);
              break;
            case 'lowKilometers':
              dataParams.column = ` round(CAST(avg(s.kilometers) as numeric), 2) `;
              dataParams.where = ` and s.kilometers > 0 `;
              ctx.body = await ScoreManager.getDateGroupedInRange(data, vehicles, dataParams);
              break;
            case 'ticketsPending':
              dataParams.column = ` round(CAST(avg(s.tickets_pending) as numeric), 2) `;
              dataParams.where = ` and s.tickets_pending > 0 `;
              ctx.body = await ScoreManager.getDateGroupedInRange(data, vehicles, dataParams);
              break;
            case 'documentsExpired':
              dataParams.column = ` round(CAST(avg(s.documents_expired) as numeric), 2) `;
              dataParams.where = ` and s.documents_expired > 0 `;
              ctx.body = await ScoreManager.getDateGroupedInRange(data, vehicles, dataParams);
              break;
            case 'documentsAboutToExpire':
              dataParams.column = ` round(CAST(avg(s.documents_on_risk) as numeric), 2) `;
              dataParams.where = ` and s.documents_on_risk > 0 `;
              ctx.body = await ScoreManager.getDateGroupedInRange(data, vehicles, dataParams);
              break;
            case 'timesBeyond':
              dataParams.column = ` round(CAST(avg(s.times_beyond_limit) as numeric), 2) `;
              dataParams.where = ` and s.times_beyond_limit > 0 `;
              ctx.body = await ScoreManager.getDateGroupedInRange(data, vehicles, dataParams);
              break;
            case 'highKilometers':
              dataParams.column = ` round(CAST(avg(s.kilometers) as numeric), 2) `;
              dataParams.where = ` and s.kilometers > 300 `;
              ctx.body = await ScoreManager.getDateGroupedInRange(data, vehicles, dataParams);
              break;
          }
    }

    public static async getLastLocation(ctx: BaseContext) {
        const params = ctx.request.body;

        ctx.body = await ScoreManager.getLastLocationByPlate(params['plate']);

    }

    public static async getPercentageDifference(ctx: BaseContext) {
        const params = ctx.request.body;
        const start = new Date();
        const end = new Date();
        const days = params['var_params']['days_query'];

        const avg = await ScoreManager.getAverageInRange(params['id'], end, start, params['var_params']['field']);
        const lastValue = await ScoreManager.getLastField(params['id'], params['var_params']['field']);
        if (lastValue.length > 0) {
            const avgRange = avg[0]['avg'];
            const lastValueField = lastValue[0]['field'];
            const percentage = ((avgRange - lastValueField) / ((avgRange + lastValueField) / 2)) * 100;

            if (percentage >= params['var_params']['greaterThan']) {
                ctx.body = 1;
            } else {
                ctx.body = 0;
            }

        } else {
            ctx.body = 0;
        }

    }

    public static async getKilometersReport(ctx: BaseContext) {
        const params = ctx.request.body;
        const tokenBearer = ctx.request.header.authorization;

        const data = {
            globalSearch: params['search'],
            endDate: params['endDate'],
            startDate: params['startDate'],
            harvest: params['harvest']
        };
        const enterprise = params['enterprise'];
        const contracts = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(enterprise, data, tokenBearer);
        const scoreByContracts =  await ScoreManager.getAverageInRangeKilometers(contracts, data.startDate, data.endDate);

        const response = await VehicleHelper.formatingEmail(params['wb'], scoreByContracts);

        ctx.body = response;
    }

    public static async getSummaryByPlate(ctx: BaseContext) {
        const params = ctx.request.body;

        ctx.body = await ScoreManager.getSummaryByPlate(params['plate']);

    }

    public static async getLastLocationReport(ctx: BaseContext) {
        const params = ctx.request.body;
        const data = {
            globalSearch: params['search'],
            endDate: params['endDate'],
            startDate: params['startDate'],
            harvest: params['harvest']
        };
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);
        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);

        ctx.body = await ScoreManager.getLastLocationReport(vehicles, data);
    }

    public static async getGpsWidget(ctx: BaseContext) {
        const params = ctx.request.body;
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);

        const data: SearchInterface = {
            globalSearch: params['search'],
            endDate: params['endDate'],
            startDate: params['startDate'],
            harvest: params['harvest']
        };

        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);

        ctx.body = {
            vehicles: vehicles,
            result: await ScoreManager.getGpsDataInRange(vehicles, data)
        };

    }

    public static async getTopSpeedWidget(ctx: BaseContext) {
        const params = ctx.request.body;
        const data: SearchInterface = {
            globalSearch: params['search'],
            endDate: params['endDate'],
            startDate: params['startDate'],
            harvest: params['harvest']
        };
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);
        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);

        ctx.body = await ScoreManager.getSpeedLimitInRange(vehicles, data);
    }

    public static async getDistanceWidgetReport(ctx: BaseContext) {
        const params = ctx.request.body;
        const data = {
            globalSearch: params['search'],
            harvest: params['harvest'],
            endDate: params['endDate'],
            startDate: params['startDate'],
            highDistance: params['highDistance']
        };
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);
        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);

        ctx.body = await ScoreManager.getDistanceHighInRangeReport(vehicles, data);
    }

    public static async getDistanceWidgetNoneReport(ctx: BaseContext) {
        const params = ctx.request.body;
        const data = {
            globalSearch: params['search'],
            harvest: params['harvest'],
            endDate: params['endDate'],
            startDate: params['startDate']
        };
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);
        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);

        ctx.body = await ScoreManager.getDistanceNoneInRangeReport(vehicles, data);
    }

    public static async getDistanceWidgetLowReport(ctx: BaseContext) {
        const params = ctx.request.body;
        const data = {
            globalSearch: params['search'],
            harvest: params['harvest'],
            endDate: params['endDate'],
            startDate: params['startDate'],
            lowDistance: params['lowDistance']
        };
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);
        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);

        ctx.body = await ScoreManager.getDistanceLowInRangeReport(vehicles, data);
    }

    public static async getDistanceWidget(ctx: BaseContext) {
        const params = ctx.request.body;
        const data: SearchInterface = {
            globalSearch: params['search'],
            harvest: params['harvest'],
            endDate: params['endDate'],
            startDate: params['startDate']
        };
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);
        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);

        ctx.body = await ScoreManager.getDistanceInRange(vehicles, data);
    }

    public static async getScoreByOwner(ctx: BaseContext) {
        const params = ctx.request.body;
        const ownerId = params['ownerId'];
        const token = ctx.request.header.authorization;
        const ownerInfo = await VehicleHelper.getOwnerInfoByOwnerId(ownerId, token);
        ctx.body = await ScoreManager.getScoreByOwner(ownerInfo['Id']);
    }

    public static async calculateSummaryTest(ctx: BaseContext) {
        ctx.body = await ScoreHelper.generateSummary();
    }

    public static async calculateScoreTestV2(ctx: BaseContext) {
        ctx.body = await ScoreHelper.generateScoreV2();
    }

    public static async calculateScoreTest(ctx: BaseContext) {
        ctx.body = await ScoreHelper.generateScore();
    }

    public static async getVehiclesInRisk(ctx: BaseContext) {
        const params = ctx.request.body;
        const data: SearchInterface = {
            globalSearch: params['search'],
            harvest: params['harvest'],
            endDate: params['endDate'],
            startDate: params['startDate']
        };
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);
        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);

        ctx.body = await ScoreManager.getVehiclesInRisk(vehicles, data);
    }

    public static async getVehiclesInRiskAll(ctx: BaseContext) {
        const params = ctx.request.body;
        const data: SearchInterface = {
            globalSearch: params['search'],
            harvest: params['harvest'],
            endDate: params['endDate'],
            startDate: params['startDate']
        };
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);
        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);

        ctx.body = await ScoreManager.getVehiclesInRiskAll(vehicles, data);
    }

    public static async getVehiclesInRiskTable(ctx: BaseContext) {
        const data: SearchInterface = ctx.query;
        const pagination: PageableInterface = ctx.state.pageable;
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);

        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);
        if (Object.keys(data).toString().length > 0) {
            const validatePagination = await PaginationHelper.validatePagination(pagination);

            if (validatePagination.toString().length === 0) {

                const validateSearch = await SearchHelper.validateSearch(data);
                if (validateSearch.toString().length === 0) {
                    const params: Object = {
                        startDate: data.startDate,
                        endDate: data.endDate,
                        vehicles: vehicles
                    };

                    const result = await ScoreManager.getVehiclesInRiskTable(params, pagination);
                    if (typeof result !== 'undefined') {
                        if (result['content'].length <= 0) {
                            const yesterday = new Date(data.endDate);
                            yesterday.setDate(yesterday.getDate() - 1);

                            const params: Object = {
                                startDate: data.startDate,
                                endDate: yesterday,
                                vehicles: vehicles
                            };

                            return ctx.body = await ScoreManager.getVehiclesInRiskTable(params, pagination);
                        } else {
                            return ctx.body = result;
                        }
                    } else {
                        await ResponseHelper.errorResponse(ctx, 'error', HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                } else {
                    await ResponseHelper.errorValidator(ctx, validateSearch, HttpStatus.UNPROCESSABLE_ENTITY);
                }
            } else {
                await ResponseHelper.errorValidator(ctx, validatePagination, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } else {
            return ResponseHelper.errorResponse(ctx, 'The query in the request is empty, please check it', HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public static async getDriversRankingAll(ctx: BaseContext) {
        const data: SearchInterface = ctx.query;
        const bodyParams = ctx.request.body;

        const pagination: PageableInterface = ctx.state.pageable;
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);

        const contracts = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);
        if (Object.keys(data).length > 0) {
            const validatePagination = await PaginationHelper.validatePagination(pagination);

            if (validatePagination.length === 0) {

                const validateSearch = await SearchHelper.validateSearch(data);

                if (validateSearch.length === 0) {
                    const params: Object = {
                        startDate: data.startDate,
                        endDate: data.endDate,
                        contracts: contracts
                    };

                    const result = await ScoreManager.getDriversRankingAll(params, pagination, bodyParams);

                    if (typeof result !== 'undefined') {
                        if (result['content'].length <= 0) {
                            const yesterday = new Date(data.endDate);
                            yesterday.setDate(yesterday.getDate() - 1);

                            const params: Object = {
                                startDate: data.startDate,
                                endDate: yesterday,
                                contracts: contracts
                            };

                            return ctx.body = await ScoreManager.getDriversRankingAll(params, pagination, bodyParams);
                        } else {
                            return ctx.body = result;
                        }
                    } else {
                        await ResponseHelper.errorResponse(ctx, 'error', HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                } else {
                    await ResponseHelper.errorValidator(ctx, validateSearch, HttpStatus.UNPROCESSABLE_ENTITY);
                }
            } else {
                await ResponseHelper.errorValidator(ctx, validatePagination, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } else {
            return ResponseHelper.errorResponse(ctx, 'The query in the request is empty, please check it', HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public static async getDriversRanking(ctx: BaseContext) {
        const data: SearchInterface = ctx.query;
        const pagination: PageableInterface = ctx.state.pageable;
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);

        const contracts = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);
        if (Object.keys(data).length > 0) {
            const validatePagination = await PaginationHelper.validatePagination(pagination);

            if (validatePagination.length === 0) {
                const validateSearch = await SearchHelper.validateSearch(data);

                if (validateSearch.length === 0) {
                    const params: Object = {
                        startDate: data.startDate,
                        endDate: data.endDate,
                        contracts: contracts
                    };

                    const result = await ScoreManager.getDriversRanking(params, pagination);

                    if (typeof result !== 'undefined') {
                        if (result['content'].length <= 0) {
                            const yesterday = new Date(data.endDate);
                            yesterday.setDate(yesterday.getDate() - 1);

                            const params: Object = {
                                startDate: data.startDate,
                                endDate: yesterday,
                                contracts: contracts
                            };

                            return ctx.body = await ScoreManager.getDriversRanking(params, pagination);
                        } else {
                            return ctx.body = result;
                        }
                    } else {
                        await ResponseHelper.errorResponse(ctx, 'error', HttpStatus.UNPROCESSABLE_ENTITY);
                    }
                } else {
                    await ResponseHelper.errorValidator(ctx, validateSearch, HttpStatus.UNPROCESSABLE_ENTITY);
                }
            } else {
                await ResponseHelper.errorValidator(ctx, validatePagination, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } else {
            return ResponseHelper.errorResponse(ctx, 'The query in the request is empty, please check it', HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public static async getVehiclesInRiskTableAll(ctx: BaseContext) {
        const data: SearchInterface = ctx.query;
        const token = ctx.request.header.authorization;
        const tokenDecode = await JwtHelper.decode(token);

        const vehicles = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(tokenDecode['enterprise'], data, token);
        if (Object.keys(data).toString().length > 0) {
            const validateSearch = await SearchHelper.validateSearch(data);
            if (validateSearch.toString().length === 0) {
                const params: Object = {
                    startDate: data.startDate,
                    endDate: data.endDate,
                    vehicles: vehicles
                };

                const result = await ScoreManager.getVehiclesInRiskTableAll(params);
                const resultFormated = await VehicleHelper.fixDataIds(result, token);

                if (typeof resultFormated !== 'undefined') {
                    return ctx.body = resultFormated;
                } else {
                    await ResponseHelper.errorResponse(ctx, 'error', HttpStatus.UNPROCESSABLE_ENTITY);
                }
            } else {
                await ResponseHelper.errorValidator(ctx, validateSearch, HttpStatus.UNPROCESSABLE_ENTITY);
            }
        } else {
            return ResponseHelper.errorResponse(ctx, 'The query in the request is empty, please check it', HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
