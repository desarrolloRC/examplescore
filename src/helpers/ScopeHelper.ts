import * as PromiseBluebird from 'bluebird';
import * as math from 'mathjs'
import { Scope } from '../entities/Scope';
import { MicroServiceHelper } from "./MicroServiceHelper";
import { VariableParameter } from "../entities/VariableParameter";
import Any = jasmine.Any;

export class ScopeHelper {
    public static async CalculateScopeByContract(scope: Scope, contract: any, token) {
        const expressionScope = {};
        try {
            await PromiseBluebird.mapSeries(scope.ScopeVariables, async function (scVar) {
                contract['variable_name'] = scVar.Variable.Name;
                contract['variable_label'] = scVar.Variable.Label;
                if (scVar.Variable.VariableParameters.length > 0) {
                    contract['var_params'] = scVar.Variable.VariableParameters[0].Parameters;
                }
                
                expressionScope[scVar.Variable.Name] = await MicroServiceHelper.doRequest(scVar.Variable.EndPoint, scVar.Variable.HttpMethod, contract, token);
                console.log(expressionScope[scVar.Variable.Name]);
                const alert = scVar.Variable.VariableParameters[0].Parameters['alert'];
                contract['variable_value'] = expressionScope[scVar.Variable.Name];
                contract['notification_type'] = 'dashboard';
                if (typeof alert !== 'undefined') {
                    if (expressionScope[scVar.Variable.Name] >= alert['greaterThanAlert']) {
                        // MicroServiceHelper.doRequest(`${process.env.MAIN_URL}process-alert`, 'POST', contract, token);
                    }
                }
                
                // console.log('uri',scVar.Variable.EndPoint + contract, scope.Name);

            }, { concurrency: 1 })
                .then(function () {
                    return true;
                });
            // console.log('value', scope.Expression, expressionScope, scope.Value);
            const resul = (math.eval(scope.Expression, expressionScope) * scope.Value);
            if (isNaN(resul)) {
                return 0;
            }
            // console.log('resul',scope.Expression, expressionScope, math.eval(scope.Expression, expressionScope));

            return resul;
        } catch (e) {
            console.log('service error', e.message);

            return 0;

        }
    }

    private static formatVariableParams(params: Array<VariableParameter>) {
        let paramsStr = '';
        for (let i = 0; i < params.length; i++) {
            if (i === 0) {
                paramsStr = paramsStr + params[i].Parameters;
            } else {
                paramsStr = paramsStr + '&' + params[i].Parameters;
            }
        }
        return paramsStr;
    }
}