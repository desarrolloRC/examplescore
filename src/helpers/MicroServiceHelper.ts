import axios from 'axios';

export class MicroServiceHelper {

    public static async doRequest(uri: string, method: string, params: any, token) {
        console.log(`${uri} ${method}`);
        console.table(params['variable_name']);
        console.table(params['variable_label']);
        try {
            let response;
            axios.defaults.timeout = 7000;
            switch (method) {
                case 'GET':
                    let query = '';
                    for (let j = 0; j < params.length; j++) {
                        const param = params[j];
                        const keys = Object.keys(param.Parameters);
                        for (let i = 0; i < keys.length; i++) {
                            if (i !== 0) {
                                query = query + '&';
                            } else {
                                query = '?';
                            }
                            query = query + keys[i] + '=' + param.Parameters[keys[i]];
                        }
                    }
                    response = await axios.get(uri + query, {
                        headers: {
                            Authorization: `${token}`
                        }
                    });
                    break;
                case 'POST':
                    response = await axios.post(uri, params, {
                        headers: {
                            Authorization: `${token}`
                        }
                    });
            }
            // return 0;
            return response.data;
        } catch (error) {
            console.log(error);
            return 0;

        }
    }
    public static async getVehicleOnInDate(params, token) {
        const response = await axios.post(process.env.GPS_URL + 'get-vehicle-on-in-date',
            {
                date: params['date'],
                plate: params['plate']

            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }


    public static async getVehicleInParkingSpot(params, token) {
        const response = await axios.post(process.env.GPS_URL + 'get-vehicle-in-parking-spot',
            {
                date: params['date'],
                plate: params['plate']

            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }


    public static async getDistanceCovered(params, token) {
        const response = await axios.post(process.env.GPS_URL + 'get-distance',
            {
                startDate: params['startDate'],
                endDate: params['endDate'],
                plate: params['plate']

            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }


    public static async getMaintenanceScore(params, token) {
        const response = await axios.post(process.env.MAINTENANCE_URL + 'get-maintenance-score',
            {
                startDate: params['startDate'],
                endDate: params['endDate'],
                vehicleId: params['vehicleId']

            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }


    public static async getOwnerContractById(ownerContractId, token) {
        const response = await axios.post(process.env.DRIVER_URL + 'get-owner-contract-by-id',
            {
                ownerContractId: ownerContractId

            },
            {headers: {
                    Authorization: `${token}`
                }});
        return response.data;
    }


    public static async getInstallmentsWithDebt(ownerContractId, token) {
        const response = await axios.post(process.env.COLLECT_URL + 'get-installments-with-debt',
            {
                ownerContractId: ownerContractId

            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }

    public static async getSpeedLimitDays(params, token) {
        const response = await axios.post(process.env.GPS_URL + 'get-speed-limit-in-range',
            {
                startDate: params['start'],
                endDate: params['end'],
                plate: params['plate'],
                speedLimit: params['speed_limit']

            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }

    public static async getAmountTicketsPending(params, token) {
        const response = await axios.post(process.env.RISK_URL + 'get-tickets-amount-pending',
            {
                vehicleId: params['vehicleId']

            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }

    public static async getAccidentsCount(params, token) {
        const response = await axios.post(process.env.RISK_URL + 'get-accidents-count',
            {
                startDate: params['start'],
                endDate: params['end'],
                vehicleId: params['vehicleId']

            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }

    public static async getCountTicketsPending(params, token) {
        const response = await axios.post(process.env.RISK_URL + 'get-tickets-count-pending',
            {
                vehicleId: params['vehicleId']

            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }

    public static async getSpeedLimitByEnterprise(params, token) {
        const response = await axios.post(process.env.DRIVER_URL + 'get-speed-limit',
            {
                enterpriseId: params['enterpriseId']

            },
            {headers: {
                    Authorization: `${token}`
                }});
        return response.data[0];
    }

    public static async getEnterprisesWithActiveVehicles(token) {

        const response = await axios.get(process.env.DRIVER_URL + 'get-enterprises', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    }

    public static async getVehiclesContractByEnterprise(enterprise: number, token) {
        const response = await axios.post(process.env.DRIVER_URL + 'get-vehicles-contracts-enterprise',
            {
                data: '',
                enterprise: enterprise
            },
            {headers: {
                    Authorization: `Bearer ${token}`
                }});
        return response.data;
    }

    public static async getVariableScore(params, token) {
        try {
            const response = await axios.post( `${params['microService']}${params['variable']['end_point']}`,
                params,
                {headers: {
                        Authorization: `${token}`
                    }});
            return response.data;
        } catch(e) {
            console.log(e);
            return 0;
        }
    }

}
