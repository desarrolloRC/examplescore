import { BaseContext } from 'koa';

export class ResponseHelper {
    public static async errorResponse(ctx: BaseContext, message: string, status: number) {
        ctx.body = {status: 'ERROR', message: message};
        ctx.status = status;
    }

    public static async errorValidator(ctx: BaseContext, validate, status: number) {
        const errorData = [];

        validate.forEach((data) => {
            errorData.push({property: data.property, constraints: data.constraints});
        });

        ctx.body = {status: 'ERROR', message: errorData};
        ctx.status = status;
    }

    public static async createResponse(ctx: BaseContext, data: any, status) {
        ctx.body = {status: 'OK', data: data};
        ctx.status = status;
    }
}
