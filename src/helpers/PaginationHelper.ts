import { validate } from 'class-validator';

import { PageableInterface } from '../interfaces/PageableInterface';
import { PageableValidator } from '../validators/PageableValidator';

export class PaginationHelper {
    public static async validatePagination(pageable: PageableInterface) {
        const validator: PageableInterface = new PageableValidator();

        validator.page = pageable.page;
        validator.size = pageable.size;
        validator.sort = pageable.sort;

        return validate(validator);
    }

    public static pagination(pageable: PageableInterface) {
        const pageNumber = pageable.page;
        const pageSize = pageable.size;
        const sort = pageable.sort;
        let sortObject = null;

        const start = +pageNumber * +pageSize;
        const end = (+pageNumber + 1) * +pageSize - 1;

        if (typeof sort !== 'undefined') {
            sortObject = sort.orders.shift();
        }

        return {pageNumber: pageNumber, limit: end - start + 1, offset: start - pageSize, sort: sortObject};
    }

    public static getSort(pagination) {
        let order = '';

        if (pagination['sort'] !== null) {
            const direction = pagination['sort']['direction'] === 'asc' ? 'ASC' : 'DESC';

            order = `ORDER BY ${pagination['sort']['property']} ${direction}`;
        }

        return order;
    }
}
