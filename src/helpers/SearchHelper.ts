import { validate } from 'class-validator';

import { SearchValidator } from '../validators/SearchValidator';
import { SearchInterface } from '../interfaces/SearchInterface';

export class SearchHelper {
    public static validateSearch(data: SearchInterface) {
        const validator: SearchInterface = new SearchValidator();

        validator.startDate = data.startDate;
        validator.endDate = data.endDate;
        validator.globalSearch = data.globalSearch;
        validator.harvest = data.harvest;

        return validate(validator);
    }
}
