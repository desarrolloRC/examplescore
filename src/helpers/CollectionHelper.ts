import axios from 'axios';

export class CollectionHelper {

    /**
     *
     * @param data
     * @param token
     */
    public static async getDebAndLastDebt(data: any, token: string) {
        return axios.post(process.env.COLLECT_URL + 'payment-and-last-payment',
            data,
            {
                headers: {
                    Authorization: token
                }
            });
    }
}
