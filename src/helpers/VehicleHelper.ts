import axios from 'axios';
import * as XLSX from 'xlsx';
import { SearchInterface } from '../interfaces/SearchInterface';

export class VehicleHelper {
    public static async fixDataIdsByContract(data: Array<any>, authorization) {
        const vehiclesIds = [];

        let vehiclesPlates = [];

        const peopleIds = [];
        let peopleDnis = [];

        data.forEach(function (value) {
            vehiclesIds.push(value['Contrato']);
        });

        if (vehiclesIds.length > 0) {
            vehiclesPlates = await VehicleHelper.getPlatesByIds(vehiclesIds, authorization);
        }

        if (vehiclesPlates.length > 0) {
            vehiclesPlates.forEach(function (value) {
                peopleIds.push(value['owner_id']);
            });
        }

        if (peopleIds.length > 0) {
            peopleDnis = await VehicleHelper.getDnisByIds(peopleIds, authorization);
        }

        const vehiclesPlatesArray = [];
        vehiclesPlates.map((value) => {
            vehiclesPlatesArray[value['id']] = value['number'];
        });

        const PlatesArray = [];
        vehiclesPlates.map((value) => {
            PlatesArray[value['id']] = value['plate'];
        });

        const DnisArray = [];
        vehiclesPlates.map((value) => {
            peopleDnis.map((valuePerson) => {
                if (value['owner_id'] == valuePerson['id']) {
                    DnisArray[value['id']] = valuePerson['dni'];
                }
            });
        });
        data.forEach(function (value, index) {
            const contract = vehiclesPlatesArray[value['Contrato']];
            const plateString = PlatesArray[value['Contrato']];
            const dni = DnisArray[value['Contrato']];

            data[index]['Contrato'] = contract;
            data[index]['Placa'] = plateString;
            data[index]['Dni'] = dni;
        });

        return data;
    }


    public static async getDnisByIds(data: Array<any>, authorization) {
        const response = await axios.post(process.env.MAIN_URL + 'get-dni-by-ids',
            {
                people: data,
            },
            {
                headers: {
                    Authorization: authorization
                }
            });

        return response.data;
    }

    public static async getVehicles(enterprise, search, token) {
        let response;

        await axios.post(process.env.DRIVER_URL + 'get-vehicles-enterprise', {
            enterprise: enterprise,
            data: search
        }, {
            headers: {
                Authorization: token
            }
        })
            .then((resp) => {
                response = resp.data;
            })
            .catch((error) => {
                console.log(error);
            });

        return response;
    }

    public static getVehicleIds(vehicles: Array<any>) {
        let vehicleId = '';

        vehicles.forEach((item, index) => {
            if (index === 0) {
                vehicleId += `${item['id']}`;
            }

            vehicleId += ',' + `${item['id']}`;
        });

        return vehicleId;
    }

    public static mergeVehicles(plates: Array<any>, data: Array<any>) {

        data.forEach((item, index) => {
            const vehicle = plates.find(vehicle => vehicle.id === item['vehicle_id']);

            data[index]['plate'] = vehicle['plate'];
            data[index]['contract_number'] = vehicle['number'];
        });

        return data;
    }

    public static async getToken() {
        const response = await axios.post(process.env.MAIN_URL_PUBLIC + 'regenerate-token', {}, {
            headers: {
                origin: `http://finantial-front.s3-website.us-east-2.amazonaws.com`
            }
        });
        return response.data['token'];
    }

    public static async getEnterprisesWithActiveVehicles(token) {

        const response = await axios.get(process.env.DRIVER_URL + 'get-enterprises', {
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        return response.data;
    }

    public static async getVehiclesContractByEnterprise(enterprise: number, token) {
        const response = await axios.post(process.env.DRIVER_URL + 'get-vehicles-contracts-enterprise',
            {
                data: '',
                enterprise: enterprise
            },
            {
                headers: {
                    Authorization: `${token}`
                }
            });
        return response.data;
    }

    public static async getDriversInfoByContract(ownerContractId: number, token) {
        const response = await axios.post(process.env.DRIVER_URL + 'get-drivers-age-by-contract',
            {
                ownerContractId: ownerContractId
            },
            {
                headers: {
                    Authorization: `${token}`
                }
            });
        return response.data;
    }

    public static async getOwnerInfoByContract(ownerContract: number, token) {
        const response = await axios.post(process.env.MAIN_URL + 'get-owner-info-by-contract',
            {
                ownerContractId: ownerContract
            },
            {
                headers: {
                    Authorization: `${token}`
                }
            });
        return response.data;
    }

    public static async getPeopleByEnterprise(enterpriseId: number, token) {
        const response = await axios.post(process.env.MAIN_URL + 'get-people-by-enterprise',
            {
                enterpriseId: enterpriseId
            },
            {
                headers: {
                    Authorization: `${token}`
                }
            });
        return response.data;
    }

    public static async getOwnerInfoByOwnerId(ownerId: number, token) {
        const response = await axios.post(process.env.DRIVER_URL + 'get-owner-contract-by-owner-id',
            {
                ownerId: ownerId
            },
            {
                headers: {
                    Authorization: `${token}`
                }
            });
        return response.data;
    }


    public static async fixDataIds(data: Array<any>, authorization) {
        const requests = data.map(async (value, index) => {
            return new Promise(async (resolve) => {
                const vehicle = await VehicleHelper.getVehicleByContract(value['owner_contract_id'], authorization);
                data[index]['owner_contract_id'] = vehicle['plate'];
                resolve();
            });
        });

        return Promise.all(requests).then(() => data);
    }

    public static async getPlatesByIds(data: Array<any>, authorization) {
        const response = await axios.post(process.env.DRIVER_URL + 'get-plates-by-ids',
            {
                vehicles: data,
            },
            {
                headers: {
                    Authorization: authorization
                }
            });

        return response.data;
    }

    public static async getVehicleByContract(ownerContractId, authorization) {
        const response = await axios.post(process.env.DRIVER_URL + 'get-vehicle-by-contract',
            {
                ownerContractId: ownerContractId,
            },
            {
                headers: {
                    Authorization: authorization
                }
            });

        return response.data[0];
    }

    public static async getCitiesById(data: Array<any>, authorization) {
        const response = await axios.post(process.env.MAIN_URL + 'get-cities-by-ids',
            {
                cities: data,
            },
            {
                headers: {
                    Authorization: authorization
                }
            });

        return response.data;
    }

    public static async getVehiclesContractByEnterpriseAndGlobalDataStringAll(enterprise: number, data, authorization) {
        const response = await axios.post(process.env.DRIVER_URL + 'get-vehicles-contracts-enterprise-all',
            {
                data: data.globalSearch,
                enterprise: enterprise,
                endDate: data.endDate,
                harvest: data.harvest
            },
            {
                headers: {
                    Authorization: authorization
                }
            });


        const vehicles = response.data;

        return this.vehicleArrayToString(vehicles, 'id');
    }

    public static async getVehiclesContractByEnterpriseAndGlobalDataString(enterprise: number, data, authorization): Promise<string> {
        const response = await axios.post(process.env.DRIVER_URL + 'get-vehicles-contracts-enterprise',
            {
                data: data.globalSearch,
                enterprise: enterprise,
                endDate: data.endDate,
                harvest: data.harvest
            },
            {
                headers: {
                    Authorization: authorization
                }
            });


        const vehicles = response.data;

        return this.vehicleArrayToString(vehicles, 'id');
    }

    /*
    * Function to convert vehicles array to string
    * */
    public static vehicleArrayToString(vehicles: Array<any>, key: string) {
        let vehiclesSimpleString = '';
        if (typeof vehicles !== 'undefined' && vehicles.length > 0) {
            vehicles.map((value, index) => {
                if (index === 0) {
                    vehiclesSimpleString = value[key];
                } else {
                    vehiclesSimpleString = vehiclesSimpleString + ',' + value[key];
                }
            });

        }
        return vehiclesSimpleString;
    }

    public static async sendNotification(data, authorization) {
        const response = await axios.post(process.env.QUEEING_URL + 'send-notification',
            data,
            {
                headers: {
                    Authorization: authorization
                }
            });
        return response.data;
    }

    public static async formatingEmail(wb, resultFormated) {
        wb.SheetNames.push('Reporte_Kms_Recorridos');
        const ws_data = resultFormated;  // a row with 2 columns
        const ws = XLSX.utils.json_to_sheet(ws_data);
        wb.Sheets['Reporte_Kms_Recorridos'] = ws;
        return wb;
    }

    public static async getContractBalance(data, token) {
        return axios.post(process.env.DRIVER_URL + 'contract-balance',
            data,
            {
                headers: {
                    Authorization: token
                }
            });
    }
}
