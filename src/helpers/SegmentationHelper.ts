import { Summary } from '../entities/Summary';

export class SegmentationHelper {

    /**
     *
     * @param data
     */
    public static mapContracts(data: any) {
        const response = [];

        data.forEach((item) => {
            response.push(item['contract_id']);
        });

        return response;
    }

    /**
     *
     * @param summaryData
     * @param contracts
     * @param paymentData
     */
    public static segmentedData(summaryData: [Summary], contracts: any, paymentData: any): { data: any[]; allData: any[] } {
        const temp = {};
        let allData = [];
        const data: any[] = [['Categoria', 'Cantidad', 'Deudas anteriores', 'Deuda a fin de mes', 'Estimación']];
        const buckets = ['0 - 30', '31 - 60', '61 - 90', '91 - 120', '121 - 150', '151 - 180', '181 - 210', '211 - 240', '241'];

        summaryData.forEach((item) => {
            temp[item['contrato']] = item;
        });
        
        const transform =  Object.keys(temp).map(key => temp[key]);

        for (const bucket of buckets) {
            let start;
            let end;
            let estimate = 0;
            let lastDebt = 0;
            let current = 0;
            const splitBucket = bucket.split('-');

            if (splitBucket.length > 1) {
                start = parseInt(splitBucket[0]);
                end = parseInt(splitBucket[1]);
            } else {
                splitBucket[0].split(' ').forEach((last, index) => end = index < 1 ? parseInt(last) : end);
            }
            
            const find = transform.filter((item) => {
                const findEstimate = contracts.find((contract) => contract['number'] === item['contrato']);
                const payment = paymentData.find((contract) => contract['owner_contract_id'] === item['id contrato']);

                if (typeof start === 'undefined') {
                    if (item['días de deuda'] >= end) {
                        item['estimado'] = findEstimate ? findEstimate['estimate'] : 0;
                        item['deuda anterior'] = payment ? payment['amount'] : 0;
                        item['deuda fin de mes'] = payment ? payment['current_debt'] : 0;
                        item['estado'] = bucket;

                        return true;
                    }
                } else {
                    if (item['días de deuda'] >= start && item['días de deuda'] <= end) {
                        item['estimado'] = findEstimate ? findEstimate['estimate'] : 0;
                        item['deuda anterior'] = payment ? payment['amount'] : 0;
                        item['deuda fin de mes'] = payment ? payment['current_debt'] : 0;
                        item['estado'] = bucket;
    
                        return true;
                    }
                }
            });

            find.forEach((item) => {
                estimate += parseFloat(item['estimado']);
                lastDebt += parseFloat(item['deuda anterior']);
                current += parseFloat(item['deuda fin de mes']);
            });

            allData = allData.concat(find);
            data.push([bucket, find.length, lastDebt, current, estimate]);
        }

        return {data: data, allData: allData};
    }
}
