import { validate } from 'class-validator';
import * as PromiseBluebird from 'bluebird';

import { SearchValidator } from '../validators/SearchValidator';
import { SearchInterface } from '../interfaces/SearchInterface';
import { VehicleHelper } from './VehicleHelper';
import { ScoreManager } from '../managers/ScoreManager';
import { MicroServiceHelper } from './MicroServiceHelper';
import objectContaining = jasmine.objectContaining;
import { type } from 'os';
import { getRepository } from 'typeorm';
import { Score } from '../entities/Score';
import { Group } from '../entities/Group';
import { ScopeManager } from '../managers/ScopeManager';
import { Scope } from '../entities/Scope';
import { ScopeScoreManager } from '../managers/ScopeScoreManager';
import { ScopeScore } from '../entities/ScopeScore';
import { ScopeHelper } from './ScopeHelper';

export class ScoreHelper {

    public static async sendResumeScore() {
        const token = await VehicleHelper.getToken();
        const tokenBearer = `Bearer ${token}`;
        const data = {
            endDate: new Date,
            globalSearch: '',
            startDate: new Date
        };
        // const enterprises = await  VehicleHelper.getEnterprisesWithActiveVehicles(token);
        const enterprises = [{'enterprise_id': 3}];
        enterprises.map(async (enterprise) => {
            const contracts = await VehicleHelper.getVehiclesContractByEnterpriseAndGlobalDataString(enterprise['enterprise_id'], data, tokenBearer);
            const scoreByContracts =  await ScoreManager.getScoreByContracts(contracts);
            if (scoreByContracts.length > 0) {
                const scoreByContractsFixed = await VehicleHelper.fixDataIds(scoreByContracts, tokenBearer);
                const enterpriseUsers = await VehicleHelper.getPeopleByEnterprise(enterprise['enterprise_id'], tokenBearer);
                enterpriseUsers.map(async (user) => {
                    const dataNotification = {
                        data: {
                            scoreByContracts: scoreByContractsFixed
                        },
                        personId: user['person_id'],
                        type: 'email',
                        messageNotificationType: 'scoreMonthly',
                        vehicle: {}
                    };
                    // await VehicleHelper.sendNotification(dataNotification, tokenBearer);
                });
            }
        });

    }

    public static async getMicroServiceUrl(type) {
        let url;
        switch (type) {
            case 'gps':
                url = process.env.GPS_URL;
                break;
            case 'risk':
                url = process.env.RISK_URL;
                break;
            case 'driving':
                url = process.env.DRIVER_URL;
                break;
            case 'maintenance':
                url = process.env.MAINTENANCE_URL;
                break;
            case 'collect':
                url = process.env.COLLECT_URL;
                break;
            case 'check':
                url = process.env.CHECK_URL;
                break;
            case 'score':
                url = process.env.SCORE_URL;
                break;
        }
        return url;
    }

    public static async generateSummaryByContract(ownerContract, variables, token, enterpriseId) {
        ownerContract['ownerInfo'] = await VehicleHelper.getOwnerInfoByContract(ownerContract['id'], token);
        
        const id = await ScoreManager.getIdSummary(ownerContract, new Date(), enterpriseId);

        return PromiseBluebird.mapSeries(variables, async function (variable, indexVariable) {
            const params = {};
            params['variable'] = variable;
            params['ownerContractId'] = ownerContract['id'];
            params['harvest'] = ownerContract['harvest'];
            params['vehicleId'] = ownerContract['vehicle_id'];
            params['variableName'] = variable['name'];
            params['plate'] = ownerContract['plate'];
            params['enterpriseId'] = enterpriseId;
            params['token'] = token;
            params['microService'] = await ScoreHelper.getMicroServiceUrl(params['variable']['type_micro_service']);
            const score = await MicroServiceHelper.getVariableScore(params, params['token']);
            console.log(score);
            console.table({
                type: typeof score,
                score: score,
                variable: variable['label'],
                plate: ownerContract['plate']
            });
            let dataSave;
            if (typeof score === 'string') {
                dataSave = {
                    id: id,
                    variable: variable['name'],
                    value: score === '' ? null : `'${score}'`
                };
            } else {
                dataSave = {
                    id: id,
                    variable: variable['name'],
                    value: score === '' ? null : score
                };

            }
            await ScoreManager.updateSummary(dataSave);
        }, {concurrency: 5})
        .then(async function () {
            return true;
        });
    }

    public static async generateScoreByContract(ownerContract, variables, speedLimit, token) {
        const lastScore = await ScoreManager.getLastScoreDateByContract(ownerContract['id']);
        let valid = true;
        if (lastScore.length > 0 && lastScore[0]['date'] !== null) {
            const date = new Date();
            const dateLast = new Date();
            const dateFormatted = `${date.getFullYear()}-${(date.getMonth() + 1)}-${date.getDate()}`;
            const dateLastFormatted = `${lastScore[0]['date'].getFullYear()}-${(lastScore[0]['date'].getMonth() + 1)}-${lastScore[0]['date'].getDate()}`;
            if (dateFormatted === dateLastFormatted) {
                valid = false;
            }
        }
        if (valid) {
            const score = await ScoreManager.createScore(ownerContract['id']);
            const scoreId = score['raw'][0]['id'];
    
            return await PromiseBluebird.mapSeries(variables, async function (variable, indexVariable) {
                const params = {};
                console.log(`Variable ${variable['label']} - ${variable['value']}`);
                params['variable'] = variable;
                params['vehicle'] = ownerContract;
                params['variableName'] = variable['name'];
                params['speed_limit'] = speedLimit['speed_limit'];
                params['token'] = token;
                params['microService'] = await ScoreHelper.getMicroServiceUrl(params['variable']['type_micro_service']);
                const score = await MicroServiceHelper.getVariableScore(params, params['token']);
                console.log(`Score ${score}`);
                const dataSave = {
                    scoreId: scoreId,
                    score: score,
                    variableId: variable['id'],
                    variableWeigth: variable['value']
                };
                await ScoreManager.createScoreVariable(dataSave);
    
            }, {concurrency: 1})
            .then(async function () {
                await ScoreHelper.calculateScoreGeneral(scoreId, variables, ownerContract['id']);
                return true;
            });
        } else {
            console.log('Score calculated for that date');
            return true;
        }
    }



    public static async generateSummaryByEnterprise(enterpriseId, token) {
        const ownerContractVehicles = await VehicleHelper.getVehiclesContractByEnterprise(enterpriseId, token);
        // const ownerContractVehicles = [{ id: 442, owner_id: 613, vehicle_id: 882, plate: 'AJA676', number: '20973'}];
        console.log(ownerContractVehicles);
        console.log(enterpriseId);
        const variables = await ScoreManager.getVariablesForSummary();
        if (variables.length > 0) {
            return await PromiseBluebird.mapSeries(ownerContractVehicles, async function (ownerContract, index) {
                console.log(`Contract ${ownerContract['id']} ${ownerContract['plate']}`);
                await ScoreHelper.generateSummaryByContract(ownerContract, variables, token, enterpriseId);
            }, {concurrency: 10})
            .then(function() {
                return true;
            });
        } else {
            console.log(`No variables found`)
            return true;
        }
    }

    public static async generateScoreV2ByEnterprise(enterpriseId, token) {
        let contracts = [];
        let individualScore = Number();
        try {
            contracts = await VehicleHelper.getVehiclesContractByEnterprise(enterpriseId, token);
            // contracts = [{ id: 464, number:'21338', vehicle_id: 904, plate: 'F9D513' , owner_id: 634}];

        } catch (e) {
            console.log(e);
        }
        const groups = await  ScoreManager.getGroupsV2ByEnterprise(enterpriseId);
        const speedLimit = await MicroServiceHelper.getSpeedLimitByEnterprise({enterpriseId : enterpriseId}, token);
        return await PromiseBluebird.mapSeries(groups, async function (group: Group) {


            return await PromiseBluebird.mapSeries(contracts, async function (contract) {
                console.table(contract);

                contract['enterpriseId'] = enterpriseId;
                let scoreObj = await ScoreManager.getLastScoreByCtrGrId(contract['id'], group.Id); // .getLastScoreByContractId(contract.id);

                if (scoreObj === undefined) {
                    scoreObj = await ScoreManager.createScoreV2(contract['id'], group.Id);
                } else {
                    scoreObj = await ScoreManager.updateScoreV2Value(scoreObj.Id, 0);
                }
                const scopes = await ScopeManager.getScopesByGroup(group.Id);
                return await PromiseBluebird.mapSeries(scopes, async function (scope: Scope) {

                    let oldScopeScore = await ScopeScoreManager.getLastScopeScoreByFk(scope.Id, scoreObj.Id);
                    let newReg = false;
                    if (oldScopeScore === undefined) {
                        oldScopeScore = new ScopeScore();
                        oldScopeScore.ScoreValue = 0;
                        newReg = true;
                    }

                    individualScore = await ScopeHelper.CalculateScopeByContract(scope, contract, token);
                    if (oldScopeScore.ScoreValue !== individualScore || newReg) {
                        await ScopeScoreManager.createScopeScore(individualScore, scope, scoreObj, group);
                    }
                }, { concurrency: 1 })
                    .then(async function () {
                        console.log('here')
                        const targetScore = await ScoreManager.getScoreByContractAndGroup(contract.id, group.Id);
                        
                        console.log('here1')
                        const scopeScoreArr = await ScopeScoreManager.getLastScopeScoreByGroupScore(group.Id, targetScore.Id);
                        
                        console.log('here2')
                        let partialGroupScore = 0;
                        for (let i = 0; i < scopeScoreArr.length; i++) {
                            const element = scopeScoreArr[i];
                            partialGroupScore = partialGroupScore + (element.ScoreValue);

                        }

                        const partialScore = parseFloat((targetScore.ScoreValue + (partialGroupScore * group.Value)).toFixed(2));
                        await ScoreManager.updateScoreV2Value(targetScore.Id, partialScore);

                        return true;
                    });
            }, { concurrency: 1 })
                .then(async function () {

                    return true;
                });


        }, { concurrency: 1 })
            .then(function () {

                return true;
            });
    }

    public static async generateScoreByEnterprise(enterpriseId, token) {
        const ownerContractVehicles = await VehicleHelper.getVehiclesContractByEnterprise(enterpriseId, token);
        // const ownerContractVehicles = [{ id: 35, vehicle_id: 225, plate: 'WPO938' }];
        console.log(ownerContractVehicles);
        console.log(enterpriseId);
        const groups = await  ScoreManager.getGroupsByEnterprise(enterpriseId);
        const speedLimit = await MicroServiceHelper.getSpeedLimitByEnterprise({enterpriseId : enterpriseId}, token);
        console.log(groups);
        return await PromiseBluebird.mapSeries(groups, async function (group) {
            const variables = await ScoreManager.getVariablesByEnterpriseAndGroup(enterpriseId, group['id']);
            if (variables.length > 0) {
                return await PromiseBluebird.mapSeries(ownerContractVehicles, async function (ownerContract) {
                    console.log(`Contract ${ownerContract['id']} ${ownerContract['plate']}`);
                    await ScoreHelper.generateScoreByContract(ownerContract, variables, speedLimit, token);
                }, {concurrency: 1})
                .then(function() {
                    return true;
                });
            } else {
                console.log(`Enterprise ${enterpriseId} has no variables`)
                return true;
            }
        }, {concurrency: 1})
        .then(function() {
            return true;
        });
    }

    public static async generateSummary() {
        const token = await VehicleHelper.getToken();
        const tokenBearer = `Bearer ${token}`;
        const t1 = new Date();
        const enterprises = await VehicleHelper.getEnterprisesWithActiveVehicles(token);
        // const enterprises = [{enterprise_id: 9}];
        return await PromiseBluebird.mapSeries(enterprises, async function (enterprise) {
            console.log(`Enterprise ${enterprise['enterprise_id']}`);
            await ScoreHelper.generateSummaryByEnterprise(enterprise['enterprise_id'], tokenBearer);
        }, {concurrency: 1})
        .then(function() {

            const t2 = new Date();
            const dif = t1.getTime() - t2.getTime();
            
            const Seconds_from_T1_to_T2 = dif / 1000;
            const Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
            console.log(`Seconds_from_T1_to_T2: ${Seconds_from_T1_to_T2}`);
            console.log(`Seconds_Between_Dates: ${Seconds_Between_Dates}`);
            return true;
        });

    }

    public static async generateScoreV2() {
        const token = await VehicleHelper.getToken();
        const tokenBearer = `Bearer ${token}`;

        // const enterprises = await  VehicleHelper.getEnterprisesWithActiveVehicles(token);
        const enterprises = [{enterprise_id: 9}];
        return await PromiseBluebird.mapSeries(enterprises, async function (enterprise) {
            console.log(`Enterprise ${enterprise['enterprise_id']}`);
            await ScoreHelper.generateScoreV2ByEnterprise(enterprise['enterprise_id'], tokenBearer);
        }, {concurrency: 1})
        .then(function() {
            return true;
        });

    }

    public static async generateScore() {
        const token = await VehicleHelper.getToken();
        const tokenBearer = `Bearer ${token}`;

        const enterprises = await  VehicleHelper.getEnterprisesWithActiveVehicles(token);
        // const enterprises = [{enterprise_id: 4}];
        return await PromiseBluebird.mapSeries(enterprises, async function (enterprise) {
            console.log(`Enterprise ${enterprise['enterprise_id']}`);
            await ScoreHelper.generateScoreByEnterprise(enterprise['enterprise_id'], tokenBearer);
        }, {concurrency: 1})
        .then(function() {
            return true;
        });

    }

    public static async calculateScoreSpeed(plate, params) {
        const start = new Date();
        const end = new Date();
        const days = params['days_query'];
        end.setDate(end.getDate() - days);

        const paramsRequest = {
            start: start,
            end: end,
            plate: plate,
            speed_limit: params['speed_limit']
        };

        const result = await MicroServiceHelper.getSpeedLimitDays(paramsRequest, params['token']);

        let count = 0;
        let countOverLimit = 0;
        if (typeof result !== 'undefined' && result.length > 0) {
            result.map((value) => {
               count ++;
               if (parseInt(value['over_limit'], 0)) {
                   countOverLimit ++;
               }
            });
        }
        return count === 0 ? 0 : Math.round(((countOverLimit * 100) / count));
    }

    public static async calculateScoreTicketsAmount(vehicleId, params) {
        const paramsRequest = {
            vehicleId: vehicleId
        };
        const result = await MicroServiceHelper.getAmountTicketsPending(paramsRequest, params['token']);

        let amount = 0;
        if (typeof result !== 'undefined' && result.length > 0) {
            amount = parseFloat(result[0]['amount']);
            if (amount > parseInt(params['limit'], 0)) {
                amount = parseInt(params['limit'], 0);
            }
        }

        return amount;
    }

    public static async calculateScoreTicketsCount(vehicleId, params) {
        const paramsRequest = {
            vehicleId: vehicleId
        };
        const result = await MicroServiceHelper.getCountTicketsPending(paramsRequest, params['token']);
        let count = 0;
        if (typeof result !== 'undefined' && result.length > 0) {
            count = parseFloat(result[0]['count']);
            if (count > parseInt(params['limit'], 0)) {
                count = parseInt(params['limit'], 0);
            }
        }

        return count;
    }


    public static async calculateScoreVehiclesOn(plate, params) {
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);

        const paramsRequest = {
            date: yesterday,
            plate: plate
        };

        const result = await MicroServiceHelper.getVehicleOnInDate(paramsRequest, params['token']);

        let vehicleOn = 0;
        if (typeof result !== 'undefined' && result.length > 0) {
            result.map((value) => {
                if (value['odometer'] > 20) {
                    vehicleOn = 1;
                }
            });
        }
        return vehicleOn;
    }

    public static async calculateScoreAccidents(vehicleId, params) {

        const start = new Date();
        const end = new Date();
        const days = params['days_query'];
        end.setDate(end.getDate() - days);

        const paramsRequest = {
            start: start,
            end: end,
            vehicleId: vehicleId
        };

        const result = await MicroServiceHelper.getAccidentsCount(paramsRequest, params['token']);

        let count = 0;
        if (typeof result !== 'undefined' && result.length > 0) {
            count = result[0]['count'];
            if (count > parseInt(params['limit'], 0)) {
                count = parseInt(params['limit'], 0);
            }
        }
        return count;
    }

    public static async calculateScoreAge(ageDrivers, params) {
        const age = ageDrivers['avg_age'];
        const limit_low = parseInt(params['limit_low'], 0);
        const limit_high = parseInt(params['limit_high'], 0);
        const center = limit_low + Math.round((limit_high - limit_low) / 2);
        const value = parseInt(params['valueVariable'], 0);
        const valueLow = 0;
        let score = 0;

        if (age >= limit_low && age <= limit_high) {
            if (age < center) {
                score = (valueLow) + (((value - valueLow) / (center - limit_low)) * (age - limit_low));
            } else if (age > center) {
                score = (value) + (((valueLow - value) / (limit_high - center)) * (age - center));
            } else {
                score = value;
            }
        } else {
            score = 0;
        }
        return score;

    }

    public static async calculateScoreVehiclesInParkingSpot(plate, params) {
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);

        const paramsRequest = {
            date: yesterday,
            plate: plate
        };

        const result = await MicroServiceHelper.getVehicleInParkingSpot(paramsRequest, params['token']);

        let vehicleIn = 0;
        if (typeof result !== 'undefined' && result.length > 0) {
            vehicleIn = 1;
        }
        return vehicleIn;
    }

    public static async calculateScoreTotalDebt(vehicleId, ownerContractId, params) {
        const paramsRequest = {
            vehicleId: vehicleId
        };
        const tickets = await MicroServiceHelper.getAmountTicketsPending(paramsRequest, params['token']);
        const collectDebt = await MicroServiceHelper.getInstallmentsWithDebt(ownerContractId, params['token']);

        let amount = 0;
        if (typeof tickets !== 'undefined' && tickets.length > 0) {
            amount = parseFloat(tickets[0]['amount']);
        }

        if (typeof collectDebt !== 'undefined' && collectDebt.length > 0) {
            collectDebt.map((value) => {
               amount += (parseFloat(value['amount']) - parseFloat(value['payed']));
            });
        }

        if (amount > parseInt(params['limit'], 0)) {
            amount = parseInt(params['limit'], 0);
        }

        return amount;
    }


    public static async calculateScoreKilometers(params, token) {
        const start = new Date();
        const end = new Date();
        const days = params['days_query'];
        end.setDate(end.getDate() - days);

        const paramsRequest = {
            startDate: end,
            endDate: start,
            plate: params['plate']
        };
        let kilometers = 0;
        const result = await MicroServiceHelper.getDistanceCovered(paramsRequest, token);
        if (typeof result !== 'undefined' && typeof result['distance'] !== 'undefined') {
            kilometers = result['distance'];
        }

        const limit_low = parseInt(params['limit_low'], 0);
        const limit_high = parseInt(params['limit_high'], 0);
        const center = limit_low + Math.round((limit_high - limit_low) / 2);
        const value = parseInt(params['valueVariable'], 0);
        const valueLow = 0;
        let score = 0;

        if (kilometers >= limit_low && kilometers <= limit_high) {
            if (kilometers < center) {
                score = (valueLow) + (((value - valueLow) / (center - limit_low)) * (kilometers - limit_low));
            } else if (kilometers > center) {
                score = (value) + (((valueLow - value) / (limit_high - center)) * (kilometers - center));
            } else {
                score = value;
            }
        } else {
            score = 0;
        }
        return score;

    }


    public static async calculateScoreMaintenance(params, token) {
        const start = new Date();
        const end = new Date();
        const days = params['days_query'];
        end.setDate(end.getDate() - days);

        const paramsRequest = {
            startDate: end,
            endDate: start,
            vehicleId: params['vehicleId']
        };
        const result = await MicroServiceHelper.getMaintenanceScore(paramsRequest, params['token']);
        let count = 0;
        let score = 0;
        let scoreTotal = 0;
        if (typeof result !== 'undefined' && result.length > 0) {
            result.map((value) => {
                count ++;
                score += parseInt(value['type'], 0);
            });
        }
        scoreTotal = count * 2;
        return count === 0 ? 0 : (score * parseInt(params['valueVariable'], 0)) / scoreTotal;

    }

    public static async calculateScoreGeneral(scoreId, variables, vehicleId) {
        const scoreObject = await getRepository(Score).findOne({Id: scoreId});
        const score = await ScoreManager.calculateScoreGeneral(scoreId);
        let totalValue = 0;
        variables.map((value) => {
            totalValue += parseInt(value['value'], 0);
        });

        const lastTime = await ScoreManager.getLastTime(scoreId, vehicleId);
        let comparisonWithLastTime = 0;
        if (typeof lastTime !== 'undefined' && lastTime.length > 0) {
            if (lastTime[0]['score_value'] === score[0]['score']) {
                comparisonWithLastTime = 0;
            } else if (lastTime[0]['score_value'] > score[0]['score']) {
                comparisonWithLastTime = -1;
            } else if (lastTime[0]['score_value'] < score[0]['score']) {
                comparisonWithLastTime = 1;
            }
        }
        let description = 'Sin cambios';
        if (comparisonWithLastTime !== 0) {
            const increasing = comparisonWithLastTime === 1 ? true : false;
            const variablesActual = await ScoreManager.getAllScoreValueByScoreId(scoreId);
            const variablesPrevious = await ScoreManager.getAllScoreValueByScoreIdPrevious(scoreId, scoreObject.OwnerContractId);
            description = await ScoreHelper.getMostAffectedVariable(variablesActual, variablesPrevious, increasing);
        }

        const data = {
          scoreId: scoreId,
          score: score[0]['score'],
          description: description,
          comparisonWithLastTime: comparisonWithLastTime
        };
        await ScoreManager.updateScoreGeneral(data);
    }

    public static async getMostAffectedVariable(variablesActual, variablesPrevious, increasing) {
        const variables = [];
        const variablesFinal = {};
        const variablesObject = {};
        return await PromiseBluebird.mapSeries(variablesActual, async function (variableActual) {
            variablesObject[variableActual['name']] = {
                actualValue: variableActual['score_value'],
                label: variableActual['label'],
                value: variableActual['value']
            };
        }, {concurrency: 1})
        .then(async function () {
            return await PromiseBluebird.mapSeries(variablesPrevious, async function (variablePrevious) {
                if (typeof variablesObject[variablePrevious['name']] !== 'undefined' && variablesObject[variablePrevious['name']] !== null) {
                    variablesObject[variablePrevious['name']]['previousValue'] = variablePrevious['score_value'];
                }
            }, {concurrency: 1})
            .then(async function () {
                return await ScoreHelper.getHighestOrLowestVariable([variablesObject], increasing);
            });
        });
    }

    public static async getHighestOrLowestVariable(variablesArray, increasing) {
        let finalVariable = 'Sin cambios';
        let variableValue = -1;
        return await PromiseBluebird.mapSeries(variablesArray, async function (variablesObject) {
            for (const property in variablesObject) {
                if (variablesObject.hasOwnProperty(property)) {
                    const variable = variablesObject[property];
                    if (increasing) {
                        if (typeof variable['previousValue'] !== 'undefined' && variable['previousValue'] !== null) {
                            if (variable['actualValue'] > variable['previousValue']) {
                                let value = Math.abs(variable['actualValue'] - variable['previousValue']);
                                value += variable['value'];
                                value += (variable['value'] - variable['actualValue']);
                                if (value > variableValue) {
                                    variableValue = value;
                                    finalVariable = variable['label'];
                                }
                            }
                        }
                    } else {
                        if (typeof variable['previousValue'] !== 'undefined' && variable['previousValue'] !== null) {
                            if (variable['actualValue'] < variable['previousValue']) {
                                let value = Math.abs(variable['actualValue'] - variable['previousValue']);
                                value += variable['value'];
                                value += (variable['value'] - variable['actualValue']);
                                if (value > variableValue) {
                                    variableValue = value;
                                    finalVariable = variable['label'];
                                }
                            }
                        }
                    }
                }
            }
        }, {concurrency: 1})
        .then(async function () {
            return finalVariable;
        });
    }

    public static async fixDataIds(data: Array<any>, authorization) {
        const vehiclesIds = [];
        const cityIds = [];
        data.map((value) => {
            vehiclesIds.push(value['owner_contract_id']);
        });

        const vehiclesPlates = await VehicleHelper.getPlatesByIds(vehiclesIds, authorization);
        const vehiclesPlatesArray = [];
        vehiclesPlates.map((value) => {
            vehiclesPlatesArray[value['id']] = value['plate'];
        });

        data.map((value, index) => {
            data[index]['owner_contract_id'] = vehiclesPlatesArray[value['owner_contract_id']];
        });

        return data;
    }
}
