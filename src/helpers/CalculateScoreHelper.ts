import axios from 'axios';
import { VehicleHelper } from './VehicleHelper';
import * as PromiseBluebird from 'bluebird';
import { ScoreManager } from '../managers/ScoreManager';

export class CalculateScoreHelper {


    public static async calculateRankingPosition(params) {
        const orderScore = await ScoreManager.getLastScoreByEnterprise(params['enterpriseId']);
        if (orderScore.length > 0) {
            let scoreFinal = 0;
            return await PromiseBluebird.mapSeries(orderScore, async function (score, index) {
                if (score['owner_contract_id'] == params['ownerContractId']) {
                    scoreFinal = (index + 1);
                }
            }, {concurrency: 1})
            .then(function() {
                return scoreFinal;
            });
        } else {
            return 0;
        }
    }

    public static async calculateRankingScore(params) {
        const orderScore = await ScoreManager.getLastScoreByEnterprise(params['enterpriseId']);

        if (orderScore.length > 0) {
            let scoreFinal = 0;
            return await PromiseBluebird.mapSeries(orderScore, async function (score, index) {
                if (score['owner_contract_id'] === params['ownerContractId']) {
                    scoreFinal = score['score_value'];
                }
            }, {concurrency: 1})
            .then(function() {
                return scoreFinal;
            });
        } else {
            return 0;
        }
    }

    public static async calculateRankingLastScore(params) {
        const orderScore = await ScoreManager.getLastScoreByEnterprise(params['enterpriseId']);

        if (orderScore.length > 0) {
            let scoreFinal = 0;
            return await PromiseBluebird.mapSeries(orderScore, async function (score, index) {
                if (score['owner_contract_id'] === params['ownerContractId']) {
                    scoreFinal = score['id'];
                }
            }, {concurrency: 1})
            .then(function() {
                return scoreFinal;
            });
        } else {
            return 0;
        }
    }

    public static async sendNotification(params) {
        try {
            const dataNotification = {
                data: {
                    amount: params['amount'],
                    messageData: params['message'],
                },
                personId: params['ownerId'],
                type: 'sms|email|push|dashboard',
                messageNotificationType: params['type'],
                vehicle: params['vehicle']
            };
            await VehicleHelper.sendNotification(dataNotification, params['authorization']);
        } catch (e) {
            console.log(e);
        }
    }

    public static async getDaysBetweenDates(firstDate, endDate) {
        const date1 = new Date(firstDate);
        const date2 = new Date(endDate);
        if (date1 <= date2) {
            const diffTime = Math.abs(date2.getTime() - date1.getTime());
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
            return diffDays;
        } else {
            return 0;
        }
    }

    public static async getDocumentValue(days, document) {
        if (days <= document['days_to_notify']) {
            if (days > 0) {
                const x0 = document['days_to_notify'];
                const x  = days;
                const x1 = 0;
                
                const y0 = 0;
                const y1 = 1;
                const y  = y0 + ( y1 - y0 ) * (( x - x0 ) / ( x1 - x0 ));
                return y * document['priority'];
            } else {
                return 0;
            }
        } else {
            return 1 * document['priority'];
        }
    }

    public static async transformArrayToIn(array) {
        let inArray = '';
        return await PromiseBluebird.mapSeries(array, async function (element, index) {
            if (index === 0) {
                inArray = `'${element}'`
            } else {
                inArray = `${inArray},'${element}'`
            }
        }, {concurrency: 1})
        .then(function() {
            return inArray;
        });
    }
}

