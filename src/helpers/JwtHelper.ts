import * as JWT from 'jsonwebtoken';

export class JwtHelper {

    public static async decode(token: string) {
        const tokenClean = token.split(' ');

        return JWT.decode(tokenClean[1]);
    }

    public static async encode(data) {

        return JWT.sign(
            data,
            process.env.SECRET_KEY,
            {
                algorithm: 'HS256',
                expiresIn: '1h',
                audience: 'https://www.taximo.co/',
                subject: 'https://www.taximo.co/',
            }
        );
    }
}
