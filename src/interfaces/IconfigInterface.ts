export interface IConfig {
    port: number;
    debugLogging: boolean;
    dbsslconn: boolean;
    databaseUrl: string;
}
