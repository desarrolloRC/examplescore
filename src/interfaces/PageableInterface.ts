export interface PageableInterface {
    page: number;
    size: number;
    sort?: any;
}
