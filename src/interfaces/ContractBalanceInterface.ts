export interface ContractBalanceInterface {
    balance: string;
    interest: string;
    percentage_estimate: string;
    estimate: string;
    contract_id: number;
    number: string;
    plate: string;
    date: string;
}
