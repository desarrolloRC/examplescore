export interface SearchInterface {
    startDate: Date;
    endDate: Date;
    globalSearch: object;
    harvest?: string;
}
