export class ContractLastDebtInterface {
    owner_contract_id: number;
    payment_date: string;
    amount: string;
    payed: number;
    current_debt: number;
    initial_debt: string;
}
