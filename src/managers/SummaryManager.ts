import { getManager, getRepository } from 'typeorm';

import { Summary } from '../entities/Summary';
import { ContractBalanceInterface } from '../interfaces/ContractBalanceInterface';
import { ContractLastDebtInterface } from '../interfaces/ContractLastDebtInterface';

export class SummaryManager {
    /**
     *
     * @param data
     * @param contracts
     */
    public static async getSegmentationData(data, contracts: string): Promise<[Summary]> {
        const queryBuilder = getManager().getRepository(Summary).createQueryBuilder();

        queryBuilder.select(`
                            date::date,
                            case 
                                when debt_days <= 30 then '0-30'
                                when debt_days > 30 and debt_days <= 60 then '30-60'
                                when debt_days > 60 and debt_days <= 90 then '60-90'
                                when debt_days > 90 then '90 +'
                            end as estado,
                            count(contract_number) as cantidad
                            
                            `);

        queryBuilder.where('date between :startDate and :endDate',
            {startDate: data.startDate, endDate: data.endDate}
        );

        if (contracts.length > 0) {
            queryBuilder.andWhere(`owner_contract_id IN (${contracts})`);
        }

        queryBuilder.groupBy(`
                            date::date,
                            case 
                                when debt_days <= 30 then '0-30'
                                when debt_days > 30 and debt_days <= 60 then '30-60'
                                when debt_days > 60 and debt_days <= 90 then '60-90'
                                when debt_days > 90 then '90 +'
                            end
                            `);

        return queryBuilder.execute();
    }

    public static async getSegmentationDataAvg(data, contracts: string): Promise<[Summary]> {
        const queryBuilder = getManager().getRepository(Summary).createQueryBuilder();

        queryBuilder.select(`
                            date::date,
                            case 
                                when debt_days <= 30 then '0-30'
                                when debt_days > 30 and debt_days <= 60 then '30-60'
                                when debt_days > 60 and debt_days <= 90 then '60-90'
                                when debt_days > 90 then '90 +'
                            end as estado,
                            avg(debt_amount) as monto
                            
                            `);

        queryBuilder.where('date between :startDate and :endDate',
            {startDate: data.startDate, endDate: data.endDate}
        );

        if (contracts.length > 0) {
            queryBuilder.andWhere(`owner_contract_id IN (${contracts})`);
        }

        queryBuilder.groupBy(`
                            date::date,
                            case 
                                when debt_days <= 30 then '0-30'
                                when debt_days > 30 and debt_days <= 60 then '30-60'
                                when debt_days > 60 and debt_days <= 90 then '60-90'
                                when debt_days > 90 then '90 +'
                            end
                            `);

        return queryBuilder.execute();
    }

    /**
     *
     * @param data
     * @param contracts
     */
    public static async getSegmentationDataReport(data, contracts: string): Promise<[Summary]> {
        const queryBuilder = getManager().getRepository(Summary).createQueryBuilder();

        queryBuilder.select(`
                            contract_number as "Contrato", 
                            debt_amount as "Valor de la deuda", 
                            debt_days as "Días de deuda", 
                            driver as "Conductor",
                            case 
                                when debt_days <= 30 then '0-30'
                                when debt_days > 30 and debt_days <= 60 then '30-60'
                                when debt_days > 60 and debt_days <= 90 then '60-90'
                                when debt_days > 90 then '90 +'
                            end as "Estado",
                            date::date
                            
                            `);

        queryBuilder.where('date between :startDate and :endDate',
            {startDate: data.startDate, endDate: data.endDate}
        );

        if (contracts.length > 0) {
            queryBuilder.andWhere(`owner_contract_id IN (${contracts})`);
        }

        queryBuilder.orderBy('vehicle_id, date');

        return queryBuilder.execute();
    }

    /**
     *
     * @param data
     * @param contracts
     */
    public static async getSegmentation(data, contracts: any): Promise<[Summary]> {
        const queryBuilder = getManager().getRepository(Summary).createQueryBuilder();

        queryBuilder.select(`owner_contract_id as "id contrato", contract_number as contrato, debt_amount as "valor de la deuda", debt_days as "días de deuda", driver as conductor`);

        queryBuilder.where('enterprise_id = :enterprise', {enterprise: data.enterprise});
        queryBuilder.andWhere('date between :startDate and :endDate',
            {startDate: data.endDate, endDate: data.endDate}
        );

        if (contracts.length > 0) {
            queryBuilder.andWhere(`owner_contract_id IN (:...contracts)`, {contracts: contracts});
        }

        queryBuilder.orderBy('vehicle_id, date');

        return queryBuilder.execute();
    }

    /**
     *
     * @param data
     */
    public static async updateContractBalance(data: ContractBalanceInterface) {
        return getManager()
            .createQueryBuilder()
            .update(Summary)
            .set({
                AccruedInterest: data.interest,
                Reserve: data.estimate,
            })
            .where('owner_contract_id = :contract', {contract: data.contract_id})
            .andWhere(`"date" = :date`, {date: data.date})
            .execute();
    }

    public static async updateContractLastDebt(data: ContractLastDebtInterface, enterprise: number) {
        return getManager()
            .createQueryBuilder()
            .update(Summary)
            .set({
                PastDueBalance: data.amount,
                InitialDebt: data.initial_debt,
            })
            .where('owner_contract_id = :contract', {contract: data.owner_contract_id})
            .andWhere(`"date" = :date`, {date: data.payment_date})
            .andWhere(`enterprise_id = :enterprise`, {enterprise: enterprise})
            .execute();
    }

    /**
     *
     * @param data
     */
    public static async updateBuckets(data: {id: number; bucket: string}) {
        return getManager()
            .createQueryBuilder()
            .update(Summary)
            .set({
                Bucket: data.bucket
            })
            .where('id = :id', {id: data.id})
            .execute();
    }

    public static async getBuckets(): Promise<{id: number; bucket: string}[]> {
        const queryBuilder = getManager().getRepository(Summary).createQueryBuilder();

        queryBuilder.select(`id,
                            (case
                            when debt_days >= 1 and debt_days <= 30 then
                            '1 - 30'
                            when debt_days >= 31 and debt_days <= 60 then
                            '31- 30'
                            when debt_days >= 61 and debt_days <= 90 then
                            '61 - 90'
                            when debt_days >= 91 and debt_days <= 120 then
                            '91 - 120'
                            when debt_days >= 121 and debt_days <= 150 then
                            '121 - 150'
                            when debt_days >= 151 and debt_days <= 180 then
                            '151 - 180'
                            when debt_days >= 181 and debt_days <= 210 then
                            '181 - 210'
                            when debt_days >= 211 and debt_days <= 240 then
                            '211 - 240'
                            when debt_days >= 241 then
                            '240 +'
                            else
                            '0'
                            end) as bucket
        `);

        queryBuilder.where('enterprise_id = 10');

        return queryBuilder.execute();
    }
}
