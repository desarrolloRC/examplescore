import { IndexablePage } from '@panderalabs/koa-pageable';
import { getConnection, getManager, getRepository } from 'typeorm';

import { Score } from '../entities/Score';
import { Group } from '../entities/Group';
import { Summary } from '../entities/Summary';
import { ScoreVariable } from '../entities/ScoreVariable';
import { PaginationHelper } from '../helpers/PaginationHelper';
import { PageableInterface } from '../interfaces/PageableInterface';

export class ScoreManager {
    public static async getPaymentScoreSummary(contract, startDate, endDate) {
        return await getManager().query(
                `
                    select id,
                           owner_contract_id,
                           payment_score
                    from summary
                    where date >= $1::date
                      and date <= $2::date
                      and owner_contract_id = $3
            `,
            [
                startDate,
                endDate,
                contract
            ]);
    }

    public static async getValueVariableByContracts(varName, contracts, date) {
        return await getManager().query(
            `
            select
                sc.owner_contract_id,
                sc.id,
                summ.enterprise_id,
                ss.scope_id ,
                (
                select
                    ssco.score_value
                from
                    scope_score ssco
                where
                    ssco.scope_id = ss.scope_id
                    and score_id = sc.id
                    and created_at = (
                    select
                        max(created_at)
                    from
                        scope_score ssco
                    where
                        ssco.scope_id = ss.scope_id
                        and score_id = sc.id) ) as score
            from
                score sc
            left join summary summ on
                sc.owner_contract_id = summ.owner_contract_id
            left join scope_score ss on
                sc.id = ss.score_id
            left join public.scope sco on
                ss.scope_id = sco.id
            where
                sc.date = $1::date
                and sco.name = $2
                and sc.owner_contract_id in (${contracts})
            group by
                sc.owner_contract_id,
                sc.id,
                summ.enterprise_id,
                ss.scope_id

		`,
            [
                date,
                varName
            ]);
    }

    public static async getDataScatterChart(var1, var2, contracts, date) {
        return await getManager().query(
            `
            select
	            s.owner_contract_id,
                coalesce(round(
                        (
                            (${var1} * 1) / ( select max(${var1}) from summary where date = $1 and owner_contract_id in (${contracts}))
                        )::numeric, 2
                    ), 0) as ${var1},
                coalesce(${var2}, 0) as ${var2}
            from
                summary s
            where
                date = $1
                and owner_contract_id in (${contracts})

		`,
            [
                date
            ]);
    }

    public static async getGpsOperationAll(contracts, startDate, endDate) {
        return await getManager().query(
            `
            select
                vehicle_id as "Contrato",
                date as "Fecha",
                coalesce(kilometers, 0) as "Recorrido",
                case
                    when coalesce(kilometers, 0) = 0 then 'Sin operacion'
                    when coalesce(kilometers, 0) > 300 then 'Sobre Operacion'
                    when coalesce(kilometers, 0) < 150 then 'Infra Operacion'
                    else 'Normal'
                end as "Categoria"
            from
                summary
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${contracts})
		`,
            [
                startDate,
                endDate
            ]);
    }

    public static async getGpsOperation(contracts, startDate, endDate) {
        return await getManager().query(
            `
            select
                count(id),
                case
                    when coalesce(kilometers, 0) = 0 then 'Sin operacion'
                    when coalesce(kilometers, 0) > 300 then 'Sobre Operacion'
                    when coalesce(kilometers, 0) < 150 then 'Infra Operacion'
                    else 'Normal'
                end as category
            from
                summary
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${contracts})
            group by
                case
                    when coalesce(kilometers, 0) = 0 then 'Sin operacion'
                    when coalesce(kilometers, 0) > 300 then 'Sobre Operacion'
                    when coalesce(kilometers, 0) < 150 then 'Infra Operacion'
                    else 'Normal'
                end
		`,
            [
                startDate,
                endDate
            ]);
    }

    public static async getChartLicensePointsCategoriesAll(contracts, startDate, endDate) {
        return await getManager().query(
            `
            select
                vehicle_id as "Contrato",
                case
                    when license_score >= 100 then '>= 100'
                    when license_score >= 40 then '>= 40'
                    when license_score < 40 then '< 40'
                    else 'Sin Puntaje'
                end as "Puntaje"
            from
                summary
            where
                owner_contract_id in(${contracts})
                and date = (
                select
                    max(date)
                from
                    summary
                where
                    date >= $1
                    and date <= $2 )

		`,
            [
                startDate,
                endDate
            ]);
    }

    public static async getChartLicensePointsCategories(contracts, startDate, endDate) {
        return await getManager().query(
            `
            select
                coalesce(count(id), 0) as count,
                case
                    when license_score >= 100 then '>= 100'
                    when license_score >= 40 then '>= 40'
                    when license_score < 40 then '< 40'
                end as category
            from
                summary
            where
                owner_contract_id in(${contracts})
                and date = (
                select
                    max(date)
                from
                    summary
                where
                    date >= $1
                    and date <= $2 )
            group by
                case
                    when license_score >= 100 then '>= 100'
                    when license_score >= 40 then '>= 40'
                    when license_score < 40 then '< 40'
                end

		`,
            [
                startDate,
                endDate
            ]);
    }

    public static async getLastScoreByEnterprise(enterpriseId) {
        return await getManager().query(
                `
                    select sc.id,
                           sc.owner_contract_id,
                           sc.score_value
                    from score sc
                             left join summary su on
                        sc.owner_contract_id = su.owner_contract_id
                    where sc.date::date = (
                        select max(date)::date
                        from score)
                      and su.enterprise_id = $1
                    group by sc.id,
                             sc.owner_contract_id,
                             sc.score_value
                    order by sc.score_value asc, sc.owner_contract_id

            `,
            [
                enterpriseId
            ]);
    }

    public static async getAverageInRange(contractId, start, end, field) {
        return await getManager().query(
            `
            select
                coalesce(avg(${field}),0) as avg
            from
                summary s
            where
                s.owner_contract_id = $1
                and s.date between $2 and $3
		`,
            [
                contractId,
                start,
                end
            ]);
    }

    public static async getAverageInRangeKilometers(contractId, start, end) {
        return await getManager().query(
            `
            select
                plate placa,
                contract_number Contrato,
                coalesce(avg(kilometers),0) as Promedio_Kilometros
            from
                summary s
            where
                s.owner_contract_id in(${contractId})
                and s.date between $1 and $2
            group by plate, contract_number
            `,
            [
                start,
                end
            ]);
    }

    public static async getLastField(contractId, field) {
        return await getManager().query(
            `
            select
                coalesce(${field},0) as field
            from
                summary s
            where
                s.owner_contract_id = $1
            order by
                s.date desc

		`,
            [
                contractId
            ]);
    }

    public static async getTimesBeyondLimitByParams(contractId, start, end) {
        return await getManager().query(
                `
                    select coalesce(sum(s.times_beyond_limit), 0) as times
                    from summary s
                    where owner_contract_id = $1
                      and s.date between $2 and $3
            `,
            [
                contractId,
                start,
                end
            ]);
    }

    public static async getScoreByContractAndGroup(ownerContractId: number, groupID: number) {
        const date = new Date();
        date.setHours(0, 0, 0, 0);
        return await getRepository(Score).findOne({
            where: {
                OwnerContractId: ownerContractId,
                groupId: groupID,
                Date: date
            }
        });
    }

    public static async updateScoreV2Value(scoreID: number, scoreValue: number) {
        const scoreRepository = await getRepository(Score);
        const scoreObj = await scoreRepository.findOne(scoreID);
        scoreObj.ScoreValue = scoreValue;
        return scoreRepository.save(scoreObj);
    }

    public static async createScoreV2(ownerContractId: number, groupID: number) {
        const date = new Date();
        date.setHours(0, 0, 0, 0);
        const scoreRepository = await getRepository(Score);
        const scoreObj = new Score();
        scoreObj.OwnerContractId = ownerContractId;
        scoreObj.ScoreValue = 0;
        scoreObj.Description = 'Calculating';
        scoreObj.groupId = groupID;
        scoreObj.CreatedAt = new Date();
        scoreObj.Date = date;
        return await scoreRepository.save(scoreObj);
    }

    public static async getLastScoreByCtrGrId(ownerContractId: number, groupID: number) {
        const date = new Date();
        date.setHours(0, 0, 0, 0);
        return await getRepository(Score).findOne({
            order: {
                CreatedAt: 'DESC'
            },
            where: {
                OwnerContractId: ownerContractId,
                groupId: groupID,
                Date: date
            }
        });
    }

    public static async getLastLocationByPlate(plate) {
        return await getManager().query(
                `
                    select s.last_date_gps as date
                    from summary s
                    where plate = $1
                    order by s.date desc
            `,
            [
                plate
            ]);
    }

    public static async getSummaryByPlate(plate) {
        return await getManager().query(
                `
                    select s.owner_contract_id,
                           s.vehicle_id,
                           s.plate,
                           s.contract_number,
                           s.debt_amount,
                           s.kilometers
                    from summary s
                    where plate = $1
                    order by s.date desc
                    limit 1
            `,
            [
                plate
            ]);
    }

    public static async getDateGroupedInRange(params, vehicles, dataParams) {
        return await getManager().query(
            `
            select
                ${dataParams['date']} as date,
                ${dataParams['column']} as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                ${dataParams['where']}
            group by
                ${dataParams['date']}
            order by
                ${dataParams['date']}
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getKilometersHighInRange(params, vehicles) {
        return await getManager().query(
            `
            select
                date_part('month', s.date) as month,
                round(CAST(avg(s.kilometers) as numeric), 2) as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and s.kilometers > 300
            group by
                date_part('month', s.date)
            order by
                date_part('month', s.date)
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getTimesBeyondLimitInRange(params, vehicles) {
        return await getManager().query(
            `
            select
                date_part('month', s.date) as month,
                round(CAST(avg(s.times_beyond_limit) as numeric), 2) as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and s.times_beyond_limit > 0
            group by
                date_part('month', s.date)
            order by
                date_part('month', s.date)
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getDocumentsAboutToExpireInRange(params, vehicles) {
        return await getManager().query(
            `
            select
                date_part('month', s.date) as month,
                round(CAST(avg(s.documents_on_risk) as numeric), 2) as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and s.documents_on_risk > 0
            group by
                date_part('month', s.date)
            order by
                date_part('month', s.date)
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getDocumentsExpiredInRange(params, vehicles) {
        return await getManager().query(
            `
            select
                date_part('month', s.date) as month,
                round(CAST(avg(s.documents_expired) as numeric), 2) as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and s.documents_expired > 0
            group by
                date_part('month', s.date)
            order by
                date_part('month', s.date)
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getTicketsPendingInRange(params, vehicles) {
        return await getManager().query(
            `
            select
                date_part('month', s.date) as month,
                round(CAST(avg(s.tickets_pending) as numeric), 2) as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and s.tickets_pending > 0
            group by
                date_part('month', s.date)
            order by
                date_part('month', s.date)
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getDistanceKilometersInRange(params, vehicles) {
        return await getManager().query(
            `
            select
                date_part('month', s.date) as month,
                round(CAST(avg(s.kilometers) as numeric), 2) as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and s.kilometers > 0
            group by
                date_part('month', s.date)
            order by
                date_part('month', s.date)
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getDebtAmountInRange(params, vehicles) {
        return await getManager().query(
            `
            select
                date_part('month', s.date) as month,
                round(CAST(avg(s.debt_amount) as numeric), 2) as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and s.debt_amount > 0
            group by
                date_part('month', s.date)
            order by
                date_part('month', s.date)
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getPaymentPercentageInRange(params, vehicles) {
        return await getManager().query(
            `
            select
                date_part('month', s.date) as month,
                round(CAST(avg(s.payment_percentage) as numeric), 2) as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
            group by
                date_part('month', s.date)
            order by
                date_part('month', s.date)
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getPaymentFrecuencyInRange(params, vehicles) {
        return await getManager().query(
            `
            select
                date_part('month', s.date) as month,
                round(CAST(avg(s.payment_freuency) as numeric), 2) as value
            from
                summary s
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
            group by
                date_part('month', s.date)
            order by
                date_part('month', s.date)
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getGpsDataInRange(vehicles, params) {
        return await getManager().query(
            `
            select
                date,
                plate,
                owner_contract_id,
                last_date_gps,
                coalesce(kilometers, 0) as kilometers
            from
                summary
            where
                date = $1
                and owner_contract_id in (${vehicles})
		`,
            [
                params['endDate']
            ]);
    }

    public static async getDistanceNoneInRangeReport(vehicles, params) {
        return await getManager().query(
            `
            select
                date as "Fecha",
                plate as "Placa",
                contract_number as "Contrato",
                coalesce(kilometers, 0) as "Kilometraje"
            from
                summary
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and kilometers = 0
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getLastLocationReport(vehicles, params) {
        return await getManager().query(
            `
            select
                date as "Fecha",
                plate as "Placa",
                contract_number as "Contrato",
                last_date_gps as "Último reporte de GPS"
            from
                summary
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getDistanceLowInRangeReport(vehicles, params) {
        return await getManager().query(
            `
            select
                date as "Fecha",
                plate as "Placa",
                contract_number as "Contrato",
                coalesce(kilometers, 0) as "Kilometraje"
            from
                summary
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and kilometers <= $3
		`,
            [
                params['startDate'],
                params['endDate'],
                params['lowDistance']
            ]);
    }

    public static async getDistanceHighInRangeReport(vehicles, params) {
        return await getManager().query(
            `
            select
                date as "Fecha",
                plate as "Placa",
                contract_number as "Contrato",
                coalesce(kilometers, 0) as "Kilometraje"
            from
                summary
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
                and kilometers >= $3
		`,
            [
                params['startDate'],
                params['endDate'],
                params['highDistance']
            ]);
    }

    public static async getSpeedLimitInRange(vehicles, params) {
        return await getManager().query(
            `
            select
                date,
                plate,
                coalesce(top_speed, 0) as top_speed,
                coalesce(times_beyond_limit, 0) as times_beyond_limit
            from
                summary
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async getDistanceInRange(vehicles, params) {
        return await getManager().query(
            `
            select
                date,
                plate,
                coalesce(kilometers, 0) as kilometers
            from
                summary
            where
                date >= $1
                and date <= $2
                and owner_contract_id in (${vehicles})
		`,
            [
                params['startDate'],
                params['endDate']
            ]);
    }

    public static async updateSummary(params) {
        const connection = getConnection();
        const queryRunner = connection.createQueryRunner();
        await queryRunner.query(
            `
            update
                summary
            set
                ${params['variable']} = ${params['value']}
            where
                id = ${params['id']}
            `
        );
        return await queryRunner.release();
    }

    public static async getIdSummary(ownerContract, date, enterpriseId) {
        console.log(ownerContract);
        const dateFormatted = new Date(date);
        const exist = await getRepository(Summary).findOne({
            OwnerContract: ownerContract['id'],
            Date: `${dateFormatted.getFullYear()}-${(dateFormatted.getMonth() + 1)}-${dateFormatted.getDate()}`
        });
        if (exist !== null && typeof (exist) !== 'undefined') {
            return exist.Id;
        } else {
            const id = await getConnection()
                .createQueryBuilder()
                .insert()
                .into(Summary)
                .values([
                    {
                        ContractNumber: ownerContract['number'],
                        Date: date,
                        Enterprise: enterpriseId,
                        OwnerContract: ownerContract['id'],
                        Harvest: ownerContract['harvest'],
                        Plate: ownerContract['plate'],
                        VehicleId: ownerContract['vehicle_id'],
                        Driver: `${ownerContract['ownerInfo'][0]['name']} ${ownerContract['ownerInfo'][0]['last_name']}`
                    }
                ])
                .execute();

            return id['raw'][0]['id'];
        }
    }

    public static async createScore(ownerContractId) {
        return await getConnection()
            .createQueryBuilder()
            .insert()
            .into(Score)
            .values([
                {
                    OwnerContractId: ownerContractId,
                    ScoreValue: 0,
                    Description: 'Calculating',
                    Date: new Date(),
                    CreatedAt: new Date(),
                }
            ])
            .execute();
    }

    public static async createScoreVariable(data) {
        return await getConnection()
            .createQueryBuilder()
            .insert()
            .into(ScoreVariable)
            .values([
                {
                    Score: data['scoreId'],
                    ScoreValue: data['score'],
                    Variable: data['variableId'],
                    VariableWeigth: data['variableWeigth'],
                    CreatedAt: new Date(),
                }
            ])
            .execute();
    }

    public static async getScoreByContracts(contracts) {
        return await getManager().query(
            `select
                        s.score_value,
                        s.description,
                        s.owner_contract_id,
                        s.date::date
                    from
                        score s
                    where
                        s."date"::date = (
                        select
                            max(date::date)
                        from
                            score) and s.owner_contract_id in (${contracts}) and s.score_value <= 35
		`);
    }

    public static async getLastScoreDateByContract(contractId) {
        return await getManager().query(
                `select s.score_value,
                        s.description,
                        s.owner_contract_id,
                        s.date::date
                 from score s
                 where s.owner_contract_id = $1
                 order by s.date::date desc
            `,
            [contractId]);
    }

    public static async getAllScoreValueByScoreId(scoreId) {
        return await getManager().query(
                `select s.id,
                        s.owner_contract_id,
                        sv.score_value,
                        sv.variable_id,
                        v.value,
                        v.label,
                        v.name
                 from score s
                          left join score_variable sv on
                     s.id = sv.score_id
                          left join variable v on
                     sv.variable_id = v.id
                 where s.id = $1
            `,
            [scoreId]);
    }

    public static async getAllScoreValueByScoreIdPrevious(scoreId, ownerContract) {
        return await getManager().query(
                `select s.id,
                        s.owner_contract_id,
                        sv.score_value,
                        sv.variable_id,
                        v.value,
                        v.label,
                        v.name
                 from score s
                          left join score_variable sv on
                     s.id = sv.score_id
                          left join variable v on
                     sv.variable_id = v.id
                 where s.id = (
                     select s.id
                     from score s
                     where owner_contract_id = $1
                       and s.id < $2
                     order by s.id desc
                     limit 1)

            `,
            [ownerContract, scoreId]);
    }

    public static async getGroupsV2ByEnterprise(enterpriseId: number) {
        return await getRepository(Group).find({
            where: {
                EnterpriseId: enterpriseId,
                Active: true
            }
        });
    }

    public static async getGroupsByEnterprise(enterpriseId) {
        return await getManager().query(
                `
                    select g.id,
                           g.name,
                           g.label
                    from public.group g
                             left join variable v on
                        g.id = v.group_id
                    where v.enterprise_id = $1
                    group by g.id,
                             g.name,
                             g.label
            `,
            [enterpriseId]);
    }

    public static async getVariablesForSummary() {
        return await getManager().query(
                `
                    select v.id,
                           v.label,
                           v.name,
                           v.type_micro_service,
                           v.end_point
                    from variable v
                             inner join category_variable cv on
                        v.id = cv.variable_id
                    where cv.active = true
                    group by v.id,
                             v.label,
                             v.name,
                             v.type_micro_service,
                             v.end_point
            `
        );
    }

    public static async getVariablesByEnterpriseAndGroup(enterpriseId, groupId) {
        return await getManager().query(
            '' +
            'select ' +
            'v.id, ' +
            'v.name, ' +
            'v.label, ' +
            'v.value, ' +
            'v.type_micro_service, ' +
            'v.end_point, ' +
            'vp.parameters ' +
            'from ' +
            'variable v ' +
            'left join variable_parameter vp on ' +
            'v.id = vp.variable_id ' +
            'where ' +
            'v.active = true ' +
            'and v.enterprise_id = $1 ' +
            'and v.group_id = $2 ' +
            '',
            [enterpriseId, groupId]);
    }

    public static async calculateScoreGeneral(scoreId) {
        return await getManager().query(
            '' +
            'select ' +
            's.id, ' +
            'round((sum(sv.score_value) * 100) / sum(v.value)) as score ' +
            'from ' +
            'score s ' +
            'left join score_variable sv on ' +
            's.id = sv.score_id ' +
            'left join variable v on ' +
            'sv.variable_id = v.id ' +
            'where ' +
            's.id = $1 ' +
            'group by ' +
            's.id' +
            '',
            [scoreId]);
    }

    public static async getVehiclesInRisk(vehicles, params) {
        return await getManager().query(
            '' +
            'select ' +
            'count(s.id) as count ' +
            'from ' +
            'score s ' +
            'where ' +
            's.date::date = $1 ' +
            'and s.score_value <= 35 and s.owner_contract_id in (' + vehicles + ')' +
            '', [
                params['endDate']
            ]);
    }

    public static async getVehiclesInRiskAll(vehicles, params) {
        return await getManager().query(
            '' +
            'select ' +
            '* ' +
            'from ' +
            'score s ' +
            'where ' +
            's.date::date = $1 and ' +
            's.owner_contract_id in (' + vehicles + ')' +
            '', [
                params['endDate']
            ]);
    }

    public static async getScoreByOwner(ownerContractId) {
        return await getManager().query(
            '' +
            'select ' +
            's.score_value ' +
            'from ' +
            'score s ' +
            'where ' +
            's.date::date = ( select max(s.date::date) from score s) ' +
            'and s.owner_contract_id = $1' +
            '', [
                ownerContractId
            ]);
    }

    public static async updateScoreGeneral(params) {
        return await getConnection()
            .createQueryBuilder()
            .update(Score)
            .set({
                ScoreValue: params['score'],
                Description: params['description'],
                ComparisonWithLastTime: params['comparisonWithLastTime']
            })
            .where('id = :id', {id: params['scoreId']})
            .execute();
    }

    public static async getBestAndWorstVariable(scoreId, totalValue) {
        return await getManager().query(
            '' +
            'select ' +
            's.id, ' +
            'v.label, ' +
            '((v.value * 100) / $1) + sv.score_value  as score_value ' +
            'from ' +
            'score s ' +
            'left join score_variable sv on ' +
            's.id = sv.score_id ' +
            'left join variable v on ' +
            'sv.variable_id = v.id ' +
            'where ' +
            's.id = $2 and sv.score_value > 0 ' +
            '',
            [
                totalValue,
                scoreId
            ]);
    }

    public static async getLastTime(scoreId, ownerContractId) {
        return await getManager().query(
            '' +
            'select ' +
            's.id, ' +
            's.score_value, ' +
            's.date ' +
            'from ' +
            'score s ' +
            'where ' +
            'id < $1 ' +
            'and owner_contract_id = $2 ' +
            'order by ' +
            's.date desc ' +
            'limit 1' +
            '',
            [
                scoreId,
                ownerContractId
            ]);
    }

    public static async getVehiclesInRiskTable(params: object, paginationData: PageableInterface) {
        const postRepository = getRepository(Score);

        let queryBuilder = postRepository.createQueryBuilder();

        const pagination = PaginationHelper.pagination(paginationData);

        queryBuilder = queryBuilder.limit(pagination.limit).offset(pagination.offset);
        if (typeof pagination.sort !== 'undefined' && pagination.sort !== null) {
            queryBuilder.orderBy(pagination.sort.property, pagination.sort.direction === 'desc' ? 'DESC' : 'ASC');
        } else {
            queryBuilder.orderBy('score_value', 'ASC');
        }

        queryBuilder.where('"Score".date::date = :endDate', {endDate: params['endDate']});
        queryBuilder.andWhere('"Score".owner_contract_id in (' + params['vehicles'] + ')');

        queryBuilder.select('' +
            '"Score".owner_contract_id, ' +
            '"Score".score_value as score, ' +
            '"Score".comparison_with_last_time as status, ' +
            '"Score".description ' +
            '');

        return new IndexablePage(await queryBuilder.getRawMany(), await queryBuilder.getCount(), paginationData);
    }

    public static async getDriversRanking(params: object, paginationData: PageableInterface) {
        const postRepository = getRepository(Summary);

        const queryBuilder = postRepository.createQueryBuilder();

        const pagination = PaginationHelper.pagination(paginationData);
        const limit = pagination.limit;
        const offset = pagination.offset;
        let order;
        let orderDirection;
        if (typeof pagination.sort !== 'undefined' && pagination.sort !== null) {
            order = ` ${pagination.sort.property} `;
            orderDirection = pagination.sort.direction === 'desc' ? ' DESC ' : ' ASC ';
        } else {
            order = ' lastvalue.ranking ';
            orderDirection = ' DESC ';
        }


        const result = await getManager().query(
            `
            select
                s.owner_contract_id,
                s.vehicle_id,
                s.plate,
                s.plate as location,
                s.driver,
                s.contract_number,
                agg.average_speed,
                agg.top_speed,
                agg.kilometers,
                agg.times_beyond_limit,
                agg.avg_check_score,
                agg.kilometers_ideal,
                agg.today_check,
                coalesce(lastvalue.harvest, 0) as harvest,
                coalesce(lastvalue.tickets_pending, 0) as tickets_pending,
                coalesce(lastvalue.amount_tickets_pending, 0) as amount_tickets_pending,
                coalesce(lastvalue.debt_amount, 0) as debt_amount,
                coalesce(lastvalue.debt_days, 0) as debt_days,
                coalesce(lastvalue.documents_expired, 0) as documents_expired,
                coalesce(lastvalue.documents_on_risk, 0) as documents_on_risk,
                coalesce(lastvalue.ideal_payment_percentage, 0) as ideal_payment_percentage,
                coalesce(lastvalue.last_check_date, 'Sin info') as last_check_date,
                coalesce(lastvalue.last_check_score, 0) as last_check_score,
                coalesce(lastvalue.last_date_gps, 'Sin info') as last_date_gps,
                coalesce(lastvalue.last_location_gps, 'Sin info') as last_location_gps,
                coalesce(lastvalue.payment_freuency, 0) as payment_freuency,
                coalesce(lastvalue.payment_percentage, 0) as payment_percentage,
                coalesce(lastvalue.ranking, 0) as ranking,
                coalesce(lastvalue.ranking_score, 0) as ranking_score,
                coalesce(s.bucket, '') as "s.bucket",
                coalesce(s.past_due_balance, 0) as "s.past_due_balance",
                coalesce(s.reserve, 0) as "s.reserve",
                coalesce(s.accrued_interest, 0) as "s.accrued_interest",
                coalesce(s.initial_debt, 0) as "s.initial_debt"
            from
                summary s
            left join (
                select
                    avg(average_speed) as average_speed,
                    avg(top_speed) as top_speed,
                    avg(last_check_score) as avg_check_score,
                    sum(kilometers) as kilometers,
                    sum(times_beyond_limit) as times_beyond_limit,
                    sum(kilometers_ideal) as kilometers_ideal,
                    sum(coalesce(today_check, 0)) as today_check,
                    s.owner_contract_id
                from
                    summary s
                where
                    s.date between $1 and $2
                group by
                    s.owner_contract_id ) agg on
                s.owner_contract_id = agg.owner_contract_id
            left join (
                select
                    s.*
                from
                    summary s
                where
                    s.date = $2) lastvalue on
                s.owner_contract_id = lastvalue.owner_contract_id
            where
                s.date between $1 and $2
                and s.owner_contract_id in (${params['contracts']})
            group by
                s.owner_contract_id,
                s.vehicle_id,
                s.plate,
                s.driver,
                s.contract_number,
                s.bucket,
                s.past_due_balance,
                s.reserve,
                s.accrued_interest,
                s.initial_debt,
                agg.average_speed,
                agg.top_speed,
                agg.kilometers,
                agg.avg_check_score,
                agg.times_beyond_limit,
                agg.kilometers_ideal,
                agg.today_check,
                lastvalue.harvest,
                lastvalue.tickets_pending,
                lastvalue.amount_tickets_pending,
                lastvalue.debt_amount,
                lastvalue.debt_days,
                lastvalue.documents_expired,
                lastvalue.documents_on_risk,
                lastvalue.ideal_payment_percentage,
                lastvalue.last_check_date,
                lastvalue.last_check_score,
                lastvalue.last_date_gps,
                lastvalue.last_location_gps,
                lastvalue.payment_freuency,
                lastvalue.payment_percentage,
                lastvalue.ranking,
                lastvalue.ranking_score
            order by
                ${order} ${orderDirection}
            limit ${limit} offset ${offset}
            `,
            [
                params['startDate'],
                params['endDate']
            ]
        );

        const resultFull = await getManager().query(
            `
            select
                s.owner_contract_id,
                s.vehicle_id,
                s.plate,
                s.plate as location,
                s.driver,
                s.contract_number,
                agg.average_speed,
                agg.top_speed,
                agg.kilometers,
                agg.times_beyond_limit,
                agg.avg_check_score,
                agg.kilometers_ideal,
                agg.today_check,
                coalesce(lastvalue.harvest, 0) as harvest,
                coalesce(lastvalue.tickets_pending, 0) as tickets_pending,
                coalesce(lastvalue.amount_tickets_pending, 0) as amount_tickets_pending,
                coalesce(lastvalue.debt_amount, 0) as debt_amount,
                coalesce(lastvalue.debt_days, 0) as debt_days,
                coalesce(lastvalue.documents_expired, 0) as documents_expired,
                coalesce(lastvalue.documents_on_risk, 0) as documents_on_risk,
                coalesce(lastvalue.ideal_payment_percentage, 0) as ideal_payment_percentage,
                coalesce(lastvalue.last_check_date, 'Sin info') as last_check_date,
                coalesce(lastvalue.last_check_score, 0) as last_check_score,
                coalesce(lastvalue.last_date_gps, 'Sin info') as last_date_gps,
                coalesce(lastvalue.last_location_gps, 'Sin info') as last_location_gps,
                coalesce(lastvalue.payment_freuency, 0) as payment_freuency,
                coalesce(lastvalue.payment_percentage, 0) as payment_percentage,
                coalesce(lastvalue.ranking, 0) as ranking,
                coalesce(lastvalue.ranking_score, 0) as ranking_score,
                coalesce(s.bucket, '') as "s.bucket",
                coalesce(s.past_due_balance, 0) as "s.past_due_balance",
                coalesce(s.reserve, 0) as "s.reserve",
                coalesce(s.accrued_interest, 0) as "s.accrued_interest",
                coalesce(s.initial_debt, 0) as "s.initial_debt"
            from summary s
            left join (
                select
                    avg(average_speed) as average_speed,
                    avg(top_speed) as top_speed,
                    avg(last_check_score) as avg_check_score,
                    sum(kilometers) as kilometers,
                    sum(times_beyond_limit) as times_beyond_limit,
                    sum(kilometers_ideal) as kilometers_ideal,
                    sum(coalesce(today_check, 0)) as today_check,
                    s.owner_contract_id
                from
                    summary s
                where
                    s.date between $1 and $2

                group by
                    s.owner_contract_id ) agg on
                s.owner_contract_id = agg.owner_contract_id
            left join (
                select
                    s.*
                from
                    summary s
                where
                    s.date = $2) lastvalue on
                s.owner_contract_id = lastvalue.owner_contract_id
            where
                s.date between $1 and $2
                and s.owner_contract_id in (${params['contracts']})
            group by
                s.owner_contract_id,
                s.vehicle_id,
                s.plate,
                s.driver,
                s.contract_number,
                s.bucket,
                s.past_due_balance,
                s.reserve,
                s.accrued_interest,
                s.initial_debt,
                agg.average_speed,
                agg.top_speed,
                agg.kilometers,
                agg.avg_check_score,
                agg.times_beyond_limit,
                agg.kilometers_ideal,
                agg.today_check,
                lastvalue.harvest,
                lastvalue.tickets_pending,
                lastvalue.amount_tickets_pending,
                lastvalue.debt_amount,
                lastvalue.debt_days,
                lastvalue.documents_expired,
                lastvalue.documents_on_risk,
                lastvalue.ideal_payment_percentage,
                lastvalue.last_check_date,
                lastvalue.last_check_score,
                lastvalue.last_date_gps,
                lastvalue.last_location_gps,
                lastvalue.payment_freuency,
                lastvalue.payment_percentage,
                lastvalue.ranking,
                lastvalue.ranking_score
            order by
                ${order} ${orderDirection}
            `,
            [
                params['startDate'],
                params['endDate']
            ]
        );
        return new IndexablePage(result, resultFull.length, paginationData);
    }

    public static async getDriversRankingAll(params: object, paginationData: PageableInterface, bodyParams: Array<any>) {
        let order;
        let select = ' ';
        let orderDirection;
        const pagination = PaginationHelper.pagination(paginationData);

        if (typeof pagination.sort !== 'undefined' && pagination.sort !== null) {
            order = ` ${pagination.sort.property} `;
            orderDirection = pagination.sort.direction === 'desc' ? ' DESC ' : ' ASC ';
        } else {
            order = ' s.owner_contract_id ';
            orderDirection = ' ASC ';
        }

        for (let j = 0; j < bodyParams.length; j++) {
            if (typeof bodyParams[j] !== 'undefined' && bodyParams[j] !== null) {
                select = select + ' ' + bodyParams[j];
            }
        }

        select = select + ' ';

        const result = await getManager().query(
            `
            select
                ` + select + `
            from
                summary s
            left join (
                select
                    avg(average_speed) as average_speed,
                    avg(top_speed) as top_speed,
                    avg(last_check_score) as avg_check_score,
                    sum(kilometers) as kilometers,
                    sum(times_beyond_limit) as times_beyond_limit,
                    sum(kilometers_ideal) as kilometers_ideal,
                    sum(coalesce(today_check, 0)) as today_check,
                    s.owner_contract_id
                from
                    summary s
                where
                    s.date between $1 and $2
                group by
                    s.owner_contract_id ) agg on
                s.owner_contract_id = agg.owner_contract_id
            left join (
                select
                    s.*
                from
                    summary s
                where
                    s.date = $2) lastvalue on
                s.owner_contract_id = lastvalue.owner_contract_id
            where
                s.date between $1 and $2
                and s.owner_contract_id in (${params['contracts']})
            group by
                s.owner_contract_id,
                s.vehicle_id,
                s.plate,
                s.driver,
                s.contract_number,
                agg.average_speed,
                agg.top_speed,
                agg.kilometers,
                agg.avg_check_score,
                agg.times_beyond_limit,
                agg.kilometers_ideal,
                agg.today_check,
                lastvalue.harvest,
                lastvalue.tickets_pending,
                lastvalue.amount_tickets_pending,
                lastvalue.debt_amount,
                lastvalue.debt_days,
                lastvalue.documents_expired,
                lastvalue.documents_on_risk,
                lastvalue.ideal_payment_percentage,
                lastvalue.last_check_date,
                lastvalue.last_check_score,
                lastvalue.last_date_gps,
                lastvalue.last_location_gps,
                lastvalue.payment_freuency,
                lastvalue.payment_percentage,
                lastvalue.engine_hours,
                lastvalue.ranking,
                lastvalue.ranking_score
            order by
                ${order} ${orderDirection}
            `,
            [
                params['startDate'],
                params['endDate']
            ]
        );

        const resultFull = await getManager().query(
            `
            select
                ` + select + `
            from
                summary s
            left join (
                select
                    avg(average_speed) as average_speed,
                    avg(top_speed) as top_speed,
                    avg(last_check_score) as avg_check_score,
                    sum(kilometers) as kilometers,
                    sum(times_beyond_limit) as times_beyond_limit,
                    sum(kilometers_ideal) as kilometers_ideal,
                    sum(coalesce(today_check, 0)) as today_check,
                    s.owner_contract_id
                from
                    summary s
                where
                    s.date between $1 and $2
                group by
                    s.owner_contract_id ) agg on
                s.owner_contract_id = agg.owner_contract_id
            left join (
                select
                    s.*
                from
                    summary s
                where
                    s.date = $2) lastvalue on
                s.owner_contract_id = lastvalue.owner_contract_id
            where
                s.date between $1 and $2
                and s.owner_contract_id in (${params['contracts']})
            group by
                s.owner_contract_id,
                s.vehicle_id,
                s.plate,
                s.driver,
                s.contract_number,
                agg.average_speed,
                agg.top_speed,
                agg.kilometers,
                agg.times_beyond_limit,
                agg.kilometers_ideal,
                agg.avg_check_score,
                agg.today_check,
                lastvalue.harvest,
                lastvalue.tickets_pending,
                lastvalue.amount_tickets_pending,
                lastvalue.debt_amount,
                lastvalue.debt_days,
                lastvalue.documents_expired,
                lastvalue.documents_on_risk,
                lastvalue.ideal_payment_percentage,
                lastvalue.last_check_date,
                lastvalue.last_check_score,
                lastvalue.last_date_gps,
                lastvalue.last_location_gps,
                lastvalue.payment_freuency,
                lastvalue.payment_percentage,
                lastvalue.engine_hours,
                lastvalue.ranking,
                lastvalue.ranking_score
            order by
                ${order} ${orderDirection}
            `,
            [
                params['startDate'],
                params['endDate']
            ]
        );

        console.log(result);

        return new IndexablePage(result, resultFull.length, paginationData);
    }

    public static async getVehiclesInRiskTableAll(params: object) {
        const postRepository = getRepository(Score);

        const queryBuilder = postRepository.createQueryBuilder();

        // queryBuilder.where('"Score".score_value <= :score', { score: 50 });
        queryBuilder.where('"Score".date::date = :endDate', {endDate: params['endDate']});
        queryBuilder.andWhere('"Score".owner_contract_id in (' + params['vehicles'] + ')');

        queryBuilder.select('' +
            '"Score".owner_contract_id, ' +
            '"Score".score_value as score, ' +
            '"Score".comparison_with_last_time as status, ' +
            '"Score".description ' +
            '');

        return await queryBuilder.getRawMany();
    }
}
