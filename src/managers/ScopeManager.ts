import {Scope} from "../entities/Scope";

export class ScopeManager{
    public static async getScopesByGroup(groupId: number){
        return await Scope.find({
            where: {
                Group: groupId
            },
            relations: [
                'ScopeVariables',
                'ScopeVariables.Variable',
                'ScopeVariables.Variable.VariableParameters'
            ]
        });

    }
}