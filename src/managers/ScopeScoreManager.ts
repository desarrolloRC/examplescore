import {ScopeScore} from "../entities/ScopeScore";
import {Scope} from "../entities/Scope";
import {Score} from "../entities/Score";
import {Group} from "../entities/Group";

export class ScopeScoreManager{
    public static async getScopeScoreByFk(scopeID: number, scoreID: number){
        return await ScopeScore.findOne({
            where:{
                Scope: {
                    Id: scopeID
                },
                Score: {
                    Id: scoreID
                }
            }
        });
    }

    public static async getLastScopeScoreByFk(scopeID: number, scoreID: number){
        return await ScopeScore.findOne({
            order: {
              CreatedAt: 'DESC'
            },
            where:{
                Scope: {
                    Id: scopeID
                },
                Score: {
                    Id: scoreID
                }
            }
        });
    }

    public static async getLastScopeScoreByGroup(groupID: number, limit: number, offset: number){
        const qb = ScopeScore.getRepository().createQueryBuilder('t1');
        qb.select();
        qb.innerJoin('scope', 'sc', 't1.scope_id = sc.id');
        qb.innerJoin('group','g','g.id = sc.group_id');
        qb.where('t1.created_at = (SELECT MAX(created_at) FROM scope_score t2 WHERE t1.id = t2.id) AND g.id = :groupID', {groupID: groupID});
        qb.limit(limit);
        qb.offset(offset);
        return await qb.execute();
    }

    public static async getLastScopeScoreByGroupScore(groupID: number,scoreID: number){
        const qb = ScopeScore.getRepository().createQueryBuilder('t1');
        qb.select('t1.id as "Id", t1.score_value as "ScoreValue", t1.created_at as "CreatedAt", sc.value as "ScopeValue", t1.score_id as "ScoreId"');
        qb.leftJoin('scope', 'sc', 't1.scope_id = sc.id');
        qb.leftJoin('group','g','g.id = sc.group_id');
        qb.where('t1.created_at = (SELECT MAX(created_at) FROM scope_score WHERE score_id = :scoreID AND scope_id = sc.id ) AND g.id = :groupID', {groupID: groupID, scoreID: scoreID});
        return await qb.execute();
    }

    public static async createScopeScore(scoreValue: number, scope: Scope, score: Score, group: Group){
        let scopeScore = new ScopeScore();
        scopeScore.ScoreValue = scoreValue;
        scopeScore.Scope = scope;
        scopeScore.Score = score;
        scopeScore.OperationData = {
          scope,
          group
        };
        return await scopeScore.save();
    }
}