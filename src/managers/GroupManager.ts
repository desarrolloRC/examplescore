import {Group} from '../entities/Group';
import { getRepository } from 'typeorm';

export class GroupManager {
    public static async getGroupsByEnterprise(enterpriseId: number){
        return await getRepository(Group).find({
            where: {
                EnterpriseId: enterpriseId
            }
        })
    }
}