import { config } from './config';
import { ConnectionOptions } from 'typeorm';
import * as PostgresConnectionStringParser from 'pg-connection-string';

// Get DB connection options from env variable
const connectionOptions = PostgresConnectionStringParser.parse(config.databaseUrl);

const dbConfig: ConnectionOptions = {
    type: 'postgres',
    host: connectionOptions.host,
    port: parseInt(connectionOptions.port.toString()),
    username: connectionOptions.user,
    password: connectionOptions.password,
    database: connectionOptions.database,
    synchronize: true,
    logging: false,
    
    entities: [
        'src/entities/**/*.ts'
    ],
    migrationsRun: true,
    migrations: [
        'src/migrations/**/*.ts'
    ],
    'cli': {
        'migrationsDir': 'src/migrations/**/*.ts'
    },
    extra: {
        ssl: config.dbsslconn, // if not development, will use SSL
    }
};

export default dbConfig;
