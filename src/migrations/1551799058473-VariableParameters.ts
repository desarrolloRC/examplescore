import { MigrationInterface, QueryRunner } from 'typeorm';

export class VariableParameters1551799058473 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `insert into variable_parameter (variable_id, parameters, active, created_at) values
                    ((select id from variable where name = 'speed'), '{"days_query": 30}', true, now()),
                    ((select id from variable where name = 'tickets_amount'), '{"limit": 450000}', true, now()),
                    ((select id from variable where name = 'tickets_count'), '{"limit": 20}', true, now()),
                    ((select id from variable where name = 'vehicles_on'), '{}', true, now()),
                    ((select id from variable where name = 'accidents'), '{"limit": 20, "days_query": 30}', true, now()),
                    ((select id from variable where name = 'age'), '{"limit_low": 20, "limit_high": 40}', true, now()),
                    ((select id from variable where name = 'address_parking'), '{}', true, now()),
                    ((select id from variable where name = 'days_without_payment'), '{"limit": 60}', true, now()),
                    ((select id from variable where name = 'debt_total'), '{"limit": 600000}', true, now()),
                    ((select id from variable where name = 'kilometers'), '{"limit_low": 4500, "limit_high": 6000, "days_query": 30}', true, now()),
                    ((select id from variable where name = 'maintenance'), '{"days_query": 30}', true, now());`
        );

        await queryRunner.query(
            `insert into variable_parameter (variable_id, parameters, active, created_at) values
                    ((select id from variable where name = 'current_guarantee'), '{"param":5}', true, now()),
                    ((select id from variable where name = 'ideal_guarantee'), '{"param":5}', true, now()),
                ((select id from variable where name = 'ina'), '{"count":5}', true, now()),
                ((select id from variable where name = 'tna'), '{"count":5}', true, now())
                    ;`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE FROM variable_parameter WHERE id > 1');
    }

}
