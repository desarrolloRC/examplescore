import { MigrationInterface, QueryRunner } from 'typeorm';

export class Variable1551799048011 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `insert into variable (label, name, value, active, enterprise_id, created_at, type_micro_service, end_point) values
                    ('Velocidad', 'speed', 2, true, 3, now(), 'gps', 'calculate-score'),
                    ('Comparendos $', 'tickets_amount', 8, true, 3, now(), 'risk', 'calculate-score'),
                    ('Comparendos #', 'tickets_count', 3, true, 3, now(), 'risk', 'calculate-score'),
                    ('Vehículos encendidos', 'vehicles_on', 7, true, 3, now(), 'gps', 'calculate-score'),
                    ('Accidentes', 'accidents', 7, true, 3, now(), 'risk', 'calculate-score'),
                    ('Edad', 'age', 2, true, 3, now(), 'driving', 'calculate-score'),
                    ('Dirección de guardado del vehículo', 'address_parking', 3, true, 3, now(), 'gps', 'calculate-score'),
                    ('Días en mora', 'days_without_payment', 7, true, 3, now(), 'driving', 'calculate-score'),
                    ('Total deuda', 'debt_total', 9, true, 3, now(), 'driving', 'calculate-score'),
                    ('Kilometros', 'kilometers', 6, true, 3, now(), 'gps', 'calculate-score'),
                    ('Mantenimiento', 'maintenance', 8, true, 3, now(), 'maintenance', 'calculate-score');`
        );
        await queryRunner.query(
            `insert into variable (label, name, value, active, created_at, type_micro_service, end_point, http_method) values
                ('Total Puntaje última inspección', 'tpi', 1, true, now(), 'inspection', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Total Puntaje Ideal última inspección', 'ti', 1, true, now(), 'inspection', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Frecuencia de Pago', 'fp', 1, true, now(), 'collect', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Cambio de frecuencia de pago', 'cfp', 1, true, now(), 'collect', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Total Inasistencias N últimos agendamientos', 'ina', 1, true, now(), 'collect', 'http://192.168.10.60:3016/api/v1/find-no-attendance/', 'GET'),
                ('Total N últimos agendamientos', 'tna', 1, true, now(), 'collect', 'http://192.168.10.60:3016/api/v1/find-schedule-number/', 'GET'),
                ('Total Días del Contrato', 'contract_days', 1, true, now(), 'collect', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Ingreso a la aplicación', 'app_acc', 1, true, now(), 'collect', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Garantia Actual', 'current_guarantee', 1, true, now(), 'collect', 'http://microservice-collection.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/get-warranty-available/', 'GET'),
                ('Garantia Ideal', 'ideal_guarantee', 1, true, now(), 'collect', 'http://microservice-collection.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/get-warranty-limit/', 'GET'),
                ('Pagado al día', 'total_current_payment', 1, true, now(), 'collect', 'http://microservice-collection.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/get-payed-until-today/', 'GET'),
                ('Deuda Actual', 'current_debt', 1, true, now(), 'collect', 'http://microservice-collection.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/get-daily-debt/', 'GET'),
                ('Deuda Actual (Total)', 'current_total_debt', 1, true, now(), 'collect', 'http://microservice-collection.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/get-all-debt/', 'GET'),
                ('Deuda Actual Gastos', 'current_expenses_debt', 1, true, now(), 'collect', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Tiempo de financiacón (Todas las opciones de pago)', 'fts', 1, true, now(), 'collect', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Tiempo Faltante del Contrato', 'missing_contract_time', 1, true, now(), 'collect', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Deuda total', 'total_debt', 1, true, now(), 'collect', 'http://api-taximo:3000/api/v1/test-score/', 'GET'),
                ('Derechos (Ahorro cuota final)', 'savings_rights', 1, true, now(), 'collect', 'http://api-taximo:3000/api/v1/test-score/', 'GET');
                    `);
        await queryRunner.query(
                `INSERT INTO "group"
                     (label, name, enterprise_id,value)
                 VALUES('Financiero', 'finance', 1,1),
                       ('Comportamiento', 'behavior', 1,1),
                       ('Estado Del Vehiculo', 'vehicle', 1,1);
                 `
        );

        await queryRunner.query(
            `INSERT INTO "scope"
                 ( "label", name, "expression", value, active, group_id)
             VALUES('Garantia', 'guarantee', 'current_guarantee / ideal_guarantee', 1, true, (select id from "group" where name = 'finance')),
                   ('Media Puntaje Inspección', 'average_score_inspection', 'tpi / ti', 1, true, (select id from "group" where name = 'vehicle')),
                   ('Frecuencia De Pagos', 'fop', 'fp', 0.2, true, (select id from "group" where name = 'behavior')),
                   ('Cambio de frecuencia de pago', 'cfop', 'cfp', 0.3, true, (select id from "group" where name = 'behavior')),
                   ('Asistencias a inspecciones y preventivo', 'ip_assistance', 'ina / tna', 0.4, true, (select id from "group" where name = 'behavior')),
                   ('Ingreso a la app', 'app_access', 'contract_days / app_acc', 0.1, true, (select id from "group" where name = 'behavior')),
                   ('Deuda total', 'total_debt', 'total_current_payment / current_total_debt', 0.058, true, (select id from "group" where name = 'finance')),
                   ('Deuda actual', 'current_debt', 'total_current_payment / current_debt', 0.176, true, (select id from "group" where name = 'finance')),
                   ('Tiempo verificación cumplimiento opciones', 'tvco', 'fts / missing_contract_time', 0.235, true, (select id from "group" where name = 'finance')),
                   ('Cobertura Total', 'total_coverage', '(total_debt - ( current_guarantee + savings_rights)) / (total_debt + current_guarantee + savings_rights)', 0.235, true, (select id from "group" where name = 'finance')),
                   ('Cobertura actual', 'current_coverage', '( current_debt - ( current_guarantee + savings_rights )) / (current_debt + current_guarantee + savings_rights)', 0.235, true, (select id from "group" where name = 'finance'));
                   `
        );
        await queryRunner.query(
            `INSERT INTO scope_variable
                 (created_at, scope_id, variable_id)
             VALUES(now(), (select id from scope where name = 'guarantee'), (select id from variable where name = 'current_guarantee')),
                (now(), (select id from scope where name = 'guarantee'), (select id from variable where name = 'ideal_guarantee')),
                   (now(), (select id from scope where name = 'average_score_inspection'), (select id from variable where name = 'tpi')),
                   (now(), (select id from scope where name = 'average_score_inspection'), (select id from variable where name = 'ti')),
                   (now(), (select id from scope where name = 'fop'), (select id from variable where name = 'fp')),
                   (now(), (select id from scope where name = 'cfop'), (select id from variable where name = 'cfp')),
                   (now(), (select id from scope where name = 'ip_assistance'), (select id from variable where name = 'ina')),
                   (now(), (select id from scope where name = 'ip_assistance'), (select id from variable where name = 'tna')),
                   (now(), (select id from scope where name = 'app_access'), (select id from variable where name = 'contract_days')),
                   (now(), (select id from scope where name = 'app_access'), (select id from variable where name = 'app_acc')),
                   (now(), (select id from scope where name = 'total_debt'), (select id from variable where name = 'total_current_payment')),
                   (now(), (select id from scope where name = 'total_debt'), (select id from variable where name = 'current_total_debt')),
                   (now(), (select id from scope where name = 'current_debt'), (select id from variable where name = 'total_current_payment')),
                   (now(), (select id from scope where name = 'current_debt'), (select id from variable where name = 'current_debt')),
                   (now(), (select id from scope where name = 'tvco'), (select id from variable where name = 'fts')),
                   (now(), (select id from scope where name = 'tvco'), (select id from variable where name = 'missing_contract_time')),
                   (now(), (select id from scope where name = 'total_coverage'), (select id from variable where name = 'total_debt')),
                   (now(), (select id from scope where name = 'total_coverage'), (select id from variable where name = 'current_guarantee')),
                   (now(), (select id from scope where name = 'total_coverage'), (select id from variable where name = 'savings_rights')),
                   (now(), (select id from scope where name = 'current_coverage'), (select id from variable where name = 'current_debt')),
                   (now(), (select id from scope where name = 'current_coverage'), (select id from variable where name = 'current_guarantee')),
                   (now(), (select id from scope where name = 'current_coverage'), (select id from variable where name = 'savings_rights'));
            `
        );

    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query('DELETE FROM variable WHERE id > 1');
    }

}
