import { MigrationInterface, QueryRunner } from "typeorm";

export class setVehicleVariables1560539563515 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `
            UPDATE "variable" SET end_point ='http://microservice-scheduling.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/find-no-attendance/' WHERE name = 'ina';
            UPDATE "variable" SET end_point = 'http://microservice-scheduling.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/find-schedule-number/' WHERE name = 'tna';
            UPDATE "variable" SET end_point = 'http://microservice-collection.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/get-all-debt/' WHERE name = 'total_debt';
            UPDATE "variable" SET end_point = 'http://microservice-collection.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/payment-frecuency/' WHERE name = 'fp';
            UPDATE "variable" SET end_point = 'http://microservice-documents.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/get-contract-days/' WHERE name = 'contract_days';
            UPDATE "variable" SET end_point = 'http://microservice-core.us-east-2.elasticbeanstalk.com/api/v1/days-logged/' WHERE name = 'app_acc';
            `);



        await queryRunner.query(
            `
            DELETE FROM "scope_variable" WHERE id IN (SELECT id FROM "scope_variable" WHERE scope_id = (SELECT id FROM "scope" WHERE name = 'average_score_inspection'));
            DELETE FROM "scope" WHERE name = 'average_score_inspection';
            UPDATE "scope" SET value = 0.4 WHERE name = 'fop';
            UPDATE "scope" SET "expression" = 'app_acc / contract_days' WHERE name = 'app_access'
           
        `);


        await queryRunner.query(
            `insert into variable (label, name, value, active, created_at, type_micro_service, end_point, http_method) values
                ('Media Puntaje Inspección', 'mpi', 1, true, now(), 'vehicle', 'http://microservice-checking.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/get-score-by-contract-evaluations/', 'GET')
                `);

        await queryRunner.query(
            `INSERT INTO "scope"
            ( "label", name, "expression", value, active, group_id) VALUES
              ('Media Puntaje Inspección', 'average_score_inspection', 'mpi', 1, true, (select id from "group" where name = 'vehicle'))
        `);

        await queryRunner.query(
            `INSERT INTO scope_variable
            (created_at, scope_id, variable_id) VALUES
            (now(), (select id from "scope" where name = 'average_score_inspection'), (select id from variable where name = 'mpi'))
        `);

        await queryRunner.query(
            `insert into variable_parameter (variable_id, parameters, active, created_at) values
            ((select id from variable where name = 'fp'), '{"last":10}', true, now())
        `);

        // http://microservice-scheduling.kxttb5eypb.us-east-2.elasticbeanstalk.com/api/v1/find-no-attendance/

       

    }



    // INSERT INTO "scope"
    //      ( "label", name, "expression", value, active, group_id)
    //  VALUES('Garantia', 'guarantee', 'current_guarantee / ideal_guarantee', 1, true, (select id from "group" where name = 'finance'))



    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
