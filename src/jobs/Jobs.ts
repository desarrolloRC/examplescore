import * as cron from 'node-cron';

import { ScoreHelper } from '../helpers/ScoreHelper';
import { SummaryManager } from '../managers/SummaryManager';

export class Jobs {

    constructor() {
        this.generalJobs();
        this.updateBuckets();
    }

    protected generalJobs() {
        cron.schedule('30 7 * * *', async () => {
            // task to execute
            console.log(`${new Date()} - Ejecucion`);
            ScoreHelper.generateScoreV2();
        });
        cron.schedule('30 13 * * *', async () => {
            // task to execute
            console.log(`${new Date()} - Ejecucion`);
            ScoreHelper.generateScoreV2();
        });
        cron.schedule('30 18 * * *', async () => {
            // task to execute
            console.log(`${new Date()} - Ejecucion`);
            ScoreHelper.generateScoreV2();
        });
        cron.schedule('30 22 * * *', async () => {
            // task to execute
            console.log(`${new Date()} - Ejecucion`);
            ScoreHelper.generateScoreV2();
        });


        cron.schedule('0 0 * * *', async () => {
            // task to execute
            console.log(`${new Date()} - Ejecucion summary`);
            ScoreHelper.generateSummary();
        });
        cron.schedule('0 5 * * *', async () => {
            // task to execute
            console.log(`${new Date()} - Ejecucion summary`);
            ScoreHelper.generateSummary();
        });
        cron.schedule('0 20 * * *', async () => {
            // task to execute
            console.log(`${new Date()} - Ejecucion summary`);
            ScoreHelper.generateSummary();
        });
        cron.schedule('0 23 * * *', async () => {
            // task to execute
            console.log(`${new Date()} - Ejecucion summary`);
            ScoreHelper.generateSummary();
        });


        // cron.schedule('0 0 4 * *', async () => {
        cron.schedule('* * * * *', async () => {
            // task to execute
            console.log(`Send score`);
            // await ScoreHelper.sendResumeScore();
        });
    }

    protected updateBuckets() {
        cron.schedule('0 2 * * *', async () => {
            console.log('start update buckets');

            const buckets = await SummaryManager.getBuckets();

            for (const item of buckets) {
                await SummaryManager.updateBuckets(item);
            }

            console.log('end update buckets');
        });
    }
}
