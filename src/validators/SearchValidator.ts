import { IsDate, IsDateString, IsNotEmpty } from 'class-validator';

export class SearchValidator {

    @IsNotEmpty()
    startDate: Date;

    @IsNotEmpty()
    endDate: Date;

    globalSearch: object;

    harvest: string;
}