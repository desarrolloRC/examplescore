import { validate } from 'class-validator';
import { BaseEntity } from 'typeorm';

export class GlobalValidator extends BaseEntity {
    /**
     * selfValidate
     */
    public async selfValidate(): Promise<Array<any>> {
        return validate(this);
    }

    /**
     * setInstance
     */
    public setInstance(instance) {
        const keys = Object.keys(instance);
        for (let i = 0; i < keys.length; i++) {
            this[keys[i]] = instance[keys[i]];
        }
    }
}
