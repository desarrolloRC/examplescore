import {
    IsInt,
    IsNotEmpty
} from 'class-validator';

export class PageableValidator {

    @IsInt()
    @IsNotEmpty()
    page: number;

    @IsInt()
    @IsNotEmpty()
    size: number;

    sort?: any;
}
