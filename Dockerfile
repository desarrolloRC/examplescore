FROM node:10.15.0-alpine
MAINTAINER Táximo
RUN mkdir /app
WORKDIR /app
COPY . /app
RUN npm install
RUN npm install -g ts-node typescript
EXPOSE 3000
CMD ["sh", "-c", "npm run build-ts && npm run watch-server"]